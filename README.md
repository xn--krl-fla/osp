to set database:

```docker-compose build && docker-compose up -d```

to start bob

```npm run bob```

Do not forget to create .env file in project root directory. Sample:

```
TOKEN=******
DEBUG=false
DEBUG_TO_DISCORD=false
DISABLED=false
MONGO_INITDB_ROOT_USERNAME=******
MONGO_INITDB_ROOT_PASSWORD=******
MONGO_INITDB_DATABASE=bob
```