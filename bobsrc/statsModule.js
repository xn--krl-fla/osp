fs = require("fs").promises;
let botInterval = 10000;

startLogging = async (generalChannels) => {
  generalChannels.map(generalChannel => {
    let stats = "";
    stats += `${Date.now()}\n`;
    const logging = async () => {
      stats += `O${generalChannel.members.filter(m => m.presence.status === 'online').size}, I${generalChannel.members.filter(m => m.presence.status === 'idle').size}\n`;
      await fs.appendFile(`./${generalChannel.guild.id}-${generalChannel.parent.id}stats.txt`, stats).then(() => stats = "");
    }
    logging()
    return setInterval(async () => {
      logging()
    }, botInterval);
  })

};

module.exports = {startLogging};