const chalk = require('chalk');

let logger = process.env.DEBUG==="true"?console.log:() => {}

logger.debug = process.env.DEBUG==="true"?(...args) => console.debug(...args.map(x => chalk.magenta(x))):() => {};
logger.info = process.env.DEBUG==="true"?(...args) => console.info(...args.map(x => chalk.blue(x))):() => {};
logger.log = process.env.DEBUG==="true"?(...args) => console.log(...args.map(x => chalk.green(x))):() => {};
logger.warn = (...args) => console.warn(...args.map(x => chalk.yellow(x)));
logger.error = (...args) => console.error(...args.map(x => chalk.red(x)));

module.exports = logger