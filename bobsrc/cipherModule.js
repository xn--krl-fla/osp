fs = require("fs").promises;

let solutions = [
    {
        pos: [8, 8],
        text: "Super, dummy úloha funguje. Ako sa máš?"
    },
    {
        pos: [48.720, 17.162],
        text: "Super, prišli ste na prvú šifru: https://drive.google.com/file/d/1lrjKgNH3RrsVTAP53kdDDkyZlvoKHu4s/"
    },
    {
        pos: [48.828885, 17.1758469],
        text: "Našli ste 2. šifru https://drive.google.com/file/d/1NDdEDOwPWD3GEtPY91V2_rhkY2xfFc7R/"
    },
    {
        pos: [48.8457782, 17.2288843],
        text: "Našli ste 3. šifru: https://drive.google.com/file/d/1sUFBy9Vv2l0ITx_BMJRYJMnxJMjyEhhi/"
    },
    {
        pos: [48.7725134, 17.317648],
        text: "Našli ste 4. šifru: https://drive.google.com/file/d/12CjrzkVoFDx01Z56vvRvkNF24P7S4Ssh/"
    },
    {
        pos: [48.7059934, 17.340538],
        text: "Našli ste 5. šifru: https://drive.google.com/file/d/1lRy_AiPu_UkthsEWGYNRY1oZ7JVqwgGh/"
    },
    {
        pos: [48.6985471, 17.5213311],
        text: "Našli ste 6. šifru: https://drive.google.com/file/d/1r9Ojg3X5GnLJjyJYm6qOiqKD72vDQ5nS/ POZOR!, riešením šifry je heslo -- jedno slovenské slovo. Napíšte ho bobovi bez diakritiky (`šifrobob <heslo>`). Ten vám potom povie polohu ďalšej šifry."
    },
    {
        pos: [48.7454495, 17.5447968],
        text: "Našli ste 7. šifru: https://drive.google.com/file/d/1JfFLY9Z739VetwVn_5PFW6pfzVKBIxV4/"
    },
    {
        pos: [48.7807343, 17.6872283],
        text: "Našli ste 8. šifru: https://drive.google.com/file/d/1BdCTKYAZvk6vLCIKFX2MZWIy_0DHXCFY/"
    },
    {
        pos: [48.7780793, 17.6929012],
        text: "Našli ste 9. šifru: https://drive.google.com/file/d/1MM4XC1zEJTvb6Vhktz1PoKv74zzzMWTg/"
    },
    {
        pos: [48.7251104, 17.7609417],
        text: "Výborne, prišli ste do cieľa, na miesto, kde sa nachádza druhý horcrux. Vašou poslednou úlohou je tento horcrux nájsť a poslať sem link naň (na fotku zavesenú na googlemaps, kde sa nachádza horcrux)."
    }
];

let hints = [
    {
        hints: ["1. hint k dummy úlohe", "2. hint k dummy úlohe", "3. hint k dummy úlohe"]
    },
    {
        hints: [
            "Hlavné mesto Slovenka je baTerka",
            "V každej otázke je medzi možnosťami schované nejaké meno. Správna odpoveď ti vyberie písmenko."
        ]
    },
    {
        hints: [
            "Asi ste si zoznamovanie s účastníkmi veľmi nevychutnali...",
            "Zaujímavé predmety to tí zakladatelia majú. Jeden má dlhší predmet, druhý zas kratší."
        ]
    },
    {
        hints: [
            "Darmo čumíš sem, tu pomoc nenájdeš. Hľadaj na mieste, kde si dostal šifru.",
            "Stále nevieš, čo doplniť za otázniky? Si pri kostole, tak sa skús pomodliť, snáď Ťa osvieti. Ani dnu nemusíš chodiť, stačí sa postaviť pred vchod."
        ]
    },
    {
        hints: [
            "Ak botanika nie je vaša silná stránka, možno matematika je, skúste spočítať krúžky.",
            "Všimli ste si niekedy, aké pekné dievčenské mená máme v kalendári? A nie len dievčenské, všetky mená sú nakoniec pekné."
        ]
    },
    {
        hints: [
            "Už ste sa poprechádzali po obci Rybky?",
            "Poštári sa predsa musia podľa niečoho orientovať, prehadzovať však čísla na písmenká našťastie nemusia."
        ]
    },
    {
        hints: [
            "V dolinách – lesný med vonia viac ako tráva! Koľko ich vlastne je?",
            "Spočítajte si verše piesne v dolinách. Prvá polovica verša znamená prvý refrén."
        ]
    },
    {
        hints: [
            "Cestovali ste niekedy prstom po mape?",
            "Pospájané mestá tvoria písmenká"
        ]
    },
    {
        hints: [
            "Nehádžte flintu do žita! Buďte čistotní, však hneď vedľa máte smetiaky!",
            "Neviete nájsť ten správny? Tak sa skúste prejsť ďalej. Možno pritom nájdete aj ďalšie stanovisko."
        ]
    },
    {
        hints: [
            "https://drive.google.com/file/d/12JL4c-nW0EGRhlWVC7xNaIkPkmm5bL2x/",
            "https://drive.google.com/file/d/1XRX7Zh-n3-7nuge-TbaoOjnwyxnIEFDt/"
        ]
    },

]

module.exports = {hints, solutions}