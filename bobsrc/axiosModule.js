//get requests

const axios = require('axios');


//https://uselessfacts.jsph.pl/random.json?language=en
const https = require('https');
const options = {
  hostname: 'uselessfacts.jsph.pl',
  port: 443,
  path: '/random.json?language=en',
  method: 'GET',
};

module.exports = () => {

// Want to use async/await? Add the `async` keyword to your outer function/method.
  async function getFact() {
    try {
      const response = await axios.get('https://uselessfacts.jsph.pl/random.json?language=en');
      return (response.data.text);
    } catch (error) {
      console.error(error);
    }
  }

  getFact().then(console.log);
  getFact().then(console.log);
  getFact().then(console.log);
};