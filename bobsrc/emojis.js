ee = require("emoji-essential")

const nameToEmojiDict = {};
const emojiToNameDict = {};
Object.keys(ee).forEach(group => {
  Object.keys(ee[group]).forEach(sub => {
    Object.keys(ee[group][sub]).forEach(emoji => {
      const key = `:${ee[group][sub][emoji].replace(/[ :]+/g, '_')}:`;
      nameToEmojiDict[key] = emoji;
      emojiToNameDict[emoji] = key;
    });
  });
});

let nameToEmoji = (name) => nameToEmojiDict[name]
let emojiToName = (emoji) => emojiToNameDict[emoji]

const reactToMsg = (id, emoji, client) => {
    client.channels.cache.map(ch=>ch&&ch.messages&&ch.messages.fetch(id).then(m=>m.react(emoji)).catch(x=>{}))
}

module.exports = {nameToEmoji,emojiToName,reactToMsg}