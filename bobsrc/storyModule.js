const logger = require("./logger");

number = 511;

const {VM} = require('vm2');
const {stories: romanStories} = require("./romanStories");
let runUserScript = (script) => {

  let inputFile = `5
1
3
5
4
2
`.split('\n').reverse();
  let outputFile = "";
  const vm = new VM({
    sandbox: {input: () => inputFile.pop(), output: (what) => outputFile += what},
    eval: false,
    wasm: false
  });
  vm.run(script);
  return outputFile;

}

stories = {
  ...romanStories,
  ...{
    mysliCislo: [
      {
        validator: msg => msg.content === "!bob:mysli" && !msg.author.bot,   //validator is condition upon which the story begins or progresses
        callback: (channel, msg) => channel.send("Myslím si číslo.")    //callback is what a bot does upon detecting a new storypoint
      }, {
        validator: msg => !isNaN(Number.parseInt(msg.content)) && !msg.bot,   //validator tries to get most recent message that passes its test
        callback: (channel, msg) => {                                         //only the callback of last valid storypoint is called
          let num = Number.parseInt(msg.content);
          if (num < number) channel.send(`Moje číslo je väčšie ako ${num}.`)
          else if (num > number) channel.send(`Moje číslo je menšie ako ${num}.`)
          return true;                                                          //return value of callback should be true if the story progressed
        }
      }, {
        validator: msg => msg.content == number && !msg.author.bot,
        callback: (channel, msg) => channel.send(`Áno, to je moje číslo! :D`)
      },
      {
        validator: (msg) => !msg.author.bot,       //this is the end of the story. It keeps bot from evaluating previous callback
        callback: () => false             //false informs upper level handler that the story did not run
      }                                   //this is needed as the bot processes last 64 messages
    ],
    teach: [
      {
        validator: msg => msg.content === "!bob:nauc" && !msg.author.bot,   //validator is condition upon which the story begins or progresses
        callback: (channel, msg) => channel.send("Ahoj! Som bob, tvoj učiteľ. Vyskúšaj ma! Skús napr. vypísať \"Ahoj Bob\" pompocou príkazu output - output(\"Ahoj, Bob\").")
      },
      {
        validator: msg => msg.content.indexOf("output") !== -1 && !msg.author.bot,
        callback: (channel, msg) => {
          channel.send(runUserScript(msg.content));
          return true;
        }
      },
      {
        validator: (msg) => !msg.author.bot,       //this is the end of the story. It keeps bot from evaluating previous callback
        callback: () => false             //false informs upper level handler that the story did not run
      }                                   //this is needed as the bot processes last 64 messages
    ],
    /*contest: [
      {
        validator: msg => msg.content.toLowerCase().indexOf("bob") !== -1 && msg.content.toLowerCase().indexOf("áno") !== -1 && !msg.author.bot,   //validator is condition upon which the story begins or progresses
        callback: (channel, msg) => channel.send("Super! Stačí sa prihlásiť cez tento formulár: https://forms.app/form/5e96c03311f91c114e002694")
      },
      {
        validator: (msg) => !msg.author.bot,       //this is the end of the story. It keeps bot from evaluating previous callback
        callback: () => false             //false informs upper level handler that the story did not run
      }                                   //this is needed as the bot processes last 64 messages
    ]*/
  }
}

let runStories = (msg) => {

  return msg.channel.messages.fetch({limit: 100}).then((messages) => {
    const EXIT_COMMAND = 'exit()'
    const messagesArray = messages.array()
    return Object.entries(stories).map(([name,story])=>{
      let storyIndex = -1;
      [...messagesArray].reverse().forEach((message,index) => {    //that trick with [...array] cost me 4 hours of debugging
        if (story[0].validator(message)) {        //special case to reset if the story begins
          storyIndex = 0;
        } else {
          // Advances in the story until `story.validator` and `story.skipWhen` return true for `message`.
          let nextStory = story[storyIndex + 1]
          while (nextStory && nextStory.validator(message)) {
            storyIndex++;
            if (!nextStory.skipWhen || !nextStory.skipWhen(message)) {
              break
            }
            nextStory = story[storyIndex + 1]
          }  
        }
        if (message.content === EXIT_COMMAND) {
          storyIndex = -1
        }
      })
      if (storyIndex < story.length && storyIndex >= 0) {
        if (storyIndex === 0) {
          msg.channel.send(`Ak ťa budem nudiť, napíš "${EXIT_COMMAND}".`)
        }
        const newestMessage = messagesArray[0]
        logger(`Story ${name} with index ${storyIndex} is being called for user ${msg.author.username}, message ${newestMessage.content}`)
        return story[storyIndex].callback(msg.channel, newestMessage);
      }
      const isLastMsgStoryRelated = messagesArray.length > 0 && messagesArray[0].content === EXIT_COMMAND
      return isLastMsgStoryRelated;
    })
  })
}
module.exports = {runStories};