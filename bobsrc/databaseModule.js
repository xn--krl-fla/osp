const url = `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@localhost:27017/`;
const mongoose = require('mongoose');

  mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}).catch(reason => {console.log(`Failed to connect to mongoDB. You have to start it with 'docker-compose build&&docker-compose up'. ${reason}`)});

  const Team = mongoose.model('Team', {
    name: String,
    channel: String,
    started: {
      type: Date,
      default: Date.now
    }
  })

  const veducichTeam = new Team({name: "vedúckovia", channel: "ešte neni"})
  veducichTeam.save().then(() => console.log('Novi veduckovia boli ulozeni.'));