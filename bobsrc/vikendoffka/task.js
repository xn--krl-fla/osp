const accents = require("remove-accents");
const constants = require("./constants")
const { getRandElem } = require("../utilities")

class Task {
  // `name: string` denotes Task"s name
  // `description: string` is a description visible to contestants
  // `checker(body: string, origMsg: Message): Bool
  constructor(name, description, checker) {
    this.name = name
    this.description = description
    this.checker = checker

  }
}

class Answer {
  // `date: Date` is time of submit
  // `body: string` text of submit
  // `attachments: Array<string>` contains URLs to attachments
  constructor(date, body, attachments) {
    this.date = date
    this.body = body
    this.attachments = attachments
  }
}

// Creates a checker that accepts any answer from found in `arr: Array<string>`.
function makeOneOfChecker(arr, helpText) {
  return async (body, origMsg) => {
    const have = accents.remove(body.trim()).toLowerCase()
    if (arr.some(x => have === accents.remove(x.trim()).toLowerCase())) {
      await origMsg.channel.send(getRandElem(constants.POSITIVE_FEEDBACK))
      return true
    } else {
      const helpMsg = (helpText) ? `\n${helpText}` : ""
      await origMsg.channel.send(getRandElem(constants.NEGATIVE_FEEDBACK) + helpMsg)
      return false
    }
  }
}

// Cretes a checker that expects `solution` in the `body`.
const makeLiteralChecker = (solution) => makeOneOfChecker([solution])

// Checks whether `origMsg` contains at least 1 attachment.
async function attachmentChecker(body, origMsg) {
  if (origMsg.attachments.array().length === 0) {
    await origMsg.channel.send("**Bob nedostal prílohu**: Táto úloha sa odovzdáva pomocou prílohy (fotka/screenshot/zipko/...).")
    return false
  } else {
    await origMsg.channel.send(getRandElem(constants.ACK_FEEDBACK))
    return true
  }
}

// Checks if submit contains non empty body (text).
async function nonEmptyChecker(body, origMsg) {
  if (body.trim().length === 0) {
    await origMsg.channel.send("**Prázdny submit**: Očakávam akýsi text, ale poslali ste mi prádzny submit :/")
    return false
  } else {
    await origMsg.channel.send(getRandElem(constants.ACK_FEEDBACK))
    return true
  }
}

const attachmentAndTextChecker = async (body, origMsg) => await attachmentChecker(body, origMsg) && await nonEmptyChecker(body, origMsg)

const pite_prvocislo = new Task("Píte prvočíslo", "Aká je hodnota 3141593. prvočísla? Odpovedajte Bobovi :)", makeLiteralChecker("52494047"))
const connection_lost = new Task("Connection Lost", "Aký rýchly máte internet? Jeden z vás ho odmerajte speedtestom a pošlite Bobovi screenshot.", attachmentChecker)
const cukrovy_zub = new Task("Cukrový zub", "Určite ste už počas karantény zjedli veľa cukroviniek. \
Ale koľko je odporúčaná dávka cukru na človeka. Pošli túto informáciu spolu so zdrojom Bobovi.", nonEmptyChecker)
const kniznica = new Task("Knižnica", "Domáca knižnica pomáha v izolácii. Akú veľkú ju máte? Postavte čo najvyššiu vežu \
z kníh a fotodôkaz pošlite Bobovi.", attachmentChecker)
const spusti_to = new Task("Spusti to!", "Aký je rozdiel medzi UEFI a BIOSom? Odpoveď pošlite bobovi.", nonEmptyChecker)
const zverinec = new Task("Zverinec", "V karanténe nám spoločnosť robia aj domáce zvieratká. Nech sa s nimi \
(s čo najviac) jeden z členov tímu odfotí a fotku pošle Bobovi.", attachmentChecker)
const zrucnost = new Task("Zručnosť", "V blížiacej sa apokalypse sa zručnosť určite zíde. Je niekto zručný z vášho tímu? \
Dokážte to, videom kde zatĺka klinec. Video pošlite bobovi.", attachmentChecker)
const zostan_zdravy = new Task("Zostaň zdravý", "A preto nos rúško. Alebo si rovno nejaké uši. Uši a pošli nám fotokoláž čo \
najkreatívnejšieho rúška a procesu šitia. Inšpiráciu nájdeš napríklad tu \
<https://thehooksite.com/wp-content/uploads/2020/01/face-masks.jpg>, <https://www.youtube.com/watch?v=_CGnFn5QF-U>.", attachmentChecker)
const umelecky_talent = new Task("Umelecký talent", "Niektorí z vedúcich radi kreslia. Že o tom nevieš? Oni sa totiž hanbia! \
Presvedč niektorého z vedúcich OŠP aby ti nakreslil obrázok. Submitni fotku a do popisu pridaj názov vedúceho.", attachmentAndTextChecker)
const korespondencne_seminare = new Task("Korešpondenčné semináre", "Určite riešite nejaký korešpondenčný seminár. Nie? No \
hybajte to napraviť. Submitnite nejakú úlohu do jedného z nich (prask.ksp.sk ak si základoškolák a ksp.sk ak si \
stredoškolák) a pošlite Bobovi svoje usernamy a akú úlohu ste submitli. Body za túto úlohu budú úmerné počtu bodov \
vašich troch submitov. Formát môže byť napr. <nick1> <uloha1> <nick2> <uloha2> <nick3> <uloha3>", nonEmptyChecker)
const neporiadok = new Task("Neporiadok", "Určite máš doma nejaký neporiadok. Uprac ho a fotograficky zdokumentujem zmenu \
pred/po upratovaním. Fotku posielajte botovi.", attachmentChecker)
const photoshop_spolu = new Task("Photoshopom spolu", "Vďaka koronavírusu túto hru musíme hrať online a ani v tíme ste sa pri \
nej osobne nestretli. To však neznamená, že nemôžete mať tímovú fotografiu! Každý sa odfoťťe a zlepte sa v nejakom grafickom \
editore a do pozadia dajte nejakú **peknú prírodu**. Fotku posielajte Bobovi.", attachmentChecker)
const logo = new Task("Logo", "Všimli ste si, že OŠP malo nedávno výmesačie? Avšak, napriek tomuto dlhému času ešte stále nemá logo. \
Vymyslite a nakreslite nám logo a pošlite ho bobovi.", attachmentChecker)
const i_like_trains = new Task("I like trains", "Chýbajú vám už vlakovýlety? Tak si ich virtuálne prežite a zistite čo sa píše na železničnej \
tabuli v Galante v aktuálnom čase. Pošlite fotodokumentáciu.", attachmentChecker)
const knihomol = new Task("Knihomoľ", "Určite ste už v karanténe prečítali už nejakú knihu. A určite aj takú ktorá obsahuje slovo koberec. \
Pošlite nám názov knihy aj s fotkou, kde sa slovo koberec nachádza.", attachmentChecker)
const kvetiny = new Task("Kvetiny", "Keď ste už nútene zavretí so svojou rodinou, zistite o nej niečo viac. Konkrétne: napíšte mi obľúbený kvietok \
každého člena vašej rodiny. Odpoveď pošlite v jednom dlhom submite Bobovi.", nonEmptyChecker)
const nezodpovedana = new Task("Nezodpovedaná", "Niektoré otázky zostávajú nezodpovedané. Koľko znakov najmenej potrebuješ dať do Googlu, aby vypľul presne nula výsledkov? \
To čo si napísal do Googlu pošli Bobovi. Nejde o to vygoogliť odpoveď, skúšajte preto hľadať čo najkratší reťazec sami.", nonEmptyChecker)
const hybrid = new Task("Hybrid", "Asi ste už všetci počuli zvesti ako sa delfíny zjavujú v benátkach. Ale nezostalo to na delfínoch. Začínajú sa \
vyskytovať prapodivné hybridy, napríklad kapybarowombaty. Pošlite Bobovi obrázok nejakého takého kapybarowombata. Môžete použiť ľubovoľný \
grafický editor.", attachmentChecker)
const basnicka = new Task("Básnička", "Existujú básne na rôzne témy. Napíšte jednu o OŠP. Čím umeleckejšia (a dlhšia) tým štedrejšie bodov \
dostanete. Svoju báseň pošlite v jednej dlhej submitovej správe.", nonEmptyChecker)
const lavosuffle = new Task("Lávosufflé", "Mnohí z nás v tejto karanténe už iste niečo piekli. Tak aký je rozdiel medzi lávovým koláčom \
a sufflé? Svoju odpoveď pošlite Bobovi v jednej dlhej správe.", nonEmptyChecker)
const kolacova = new Task("Koláčová", "Pri riešení tejto hry ste už určite vyhladli! Tak čo tak si rýchlo napiecť koláč. Stačí k \
tomu mikrovlná rúra a nejaký ten pohár. Môžete sa inšpirovať napríklad tu <https://tasty.co/compilation/13-easy-microwave-cakes>. \
Ako submit odovzdajte fotokoláž vášho koláča a procesu prípravy. PS: Ak nemáte nikto mikrovlnku, vytvorte nejakú inú cukrovinu.", attachmentChecker)
const miro_jaros_1 = new Task("Miro Jaroš 1", "V ktorom meste sa natáčal videoklip Mira Jaroša Strašidlá? Odpoveď napíš Bobovi.",
  makeLiteralChecker("Malacky"))
const miro_jaros_2 = new Task("Miro Jaroš 2", "V ktorej piesni Mira Jaroša sa spomína Plavecký Štvrtok?  Odpoveď napíš Bobovi.",
  makeLiteralChecker("Planety"))
const miro_jaros_3 = new Task("Miro Jaroš 3", "Ako sa volá predmet, pomocou ktorého sa dostal Soplík lolík z Hankiného nošteka? \
Odpoveď v nominatíve singuláru (dve slová) napíš Bobovi. <https://www.youtube.com/watch?v=3E78l2ZP6S8>", makeLiteralChecker("nosna sprcha"))
const dosli = new Task("Došli", "Došli questy. Tak nám nejaký vymyslite a pošlite Bobovi.", nonEmptyChecker)
const anketa = new Task("Anketa", "Bob má veľmi rád ankety. Pošlite mu (všetci) svoj feedback v tejto ankete: \
<https://forms.app/form/5e99f60aa32b315548ac13cf>. Keď budete hotoví, submitnite text `zadarmo`.", makeLiteralChecker("zadarmo"))
const bob_krici_1 = new Task("Bob kričí 1", "Napíšte Bobovi do **súkromnej** správy `!bob:klobuk` a zahrajte sa s ním hru. Submitnite číslo, \
ktorým ste ho donútili vypísať 123456.", makeLiteralChecker("244466666"))
const bob_krici_2 = new Task("Bob kričí 2", "Napíšte Bobovi do **súkromnej** správy `!bob:jahoda` a zahrajte sa s ním hru. Submitnite číslo, \
ktorým ste ho donútili vypísať 18181818.", makeLiteralChecker("17957351"))
const narodeniny = new Task("Narodeniny", "Každý z vás má nejaké narodeniny. A na narodeniny určite máte nejakú (možno karanténnu) oslavu. \
Koľko najmenej dní ubehne medzi niektorými vašimi narodeninovými oslavami? Odpoveď posielajte bobovi. Pošlite bobovi v jednej správe dátumy \
vašich narodenín a najmenší počtet dní medzi oslavami.", nonEmptyChecker)
const gulocka = new Task("Guľôčka", "Chcel by som guľôčku. Z papiera alebo alobalu. Čo najguľatejšiu. Odovzdávajte fotodokumentáciu.", attachmentChecker)
const pesnicka_1 = new Task("Pesnička 1", "Bob sa pomýlil a hodil **slovenskú** pesničku, ktorú má veľmi rád do google prekladaču a tá sa \
preložila do angličtiny. Bob ale zabudol, ako sa volala. Rád by si ju ale vypočul. Napíšte Bobovi, ako sa pesnička volá. Nepíšte interpreta, iba názov. \
Takto znie preložený text:\n\n```keywords Hairstyles judge us Hear someone sob ordinary Elevator only average Empty days clowns They are judging us By \
irritating pictures and phrases And I hope more in us```", makeLiteralChecker("nie sme zlí"))
const pesnicka_2 = new Task("Pesnička 2", "Bob sa pomýlil a hodil **slovenskú** pesničku, ktorá sa mu veľmi nepáči do google prekladaču a tá sa preložila \
do angličtiny. Bob ale zabudol, ako sa volala. Rád by ju poslal svojmu úhlavnému nepriateľovi Lolovi, aby ňou tiež trpel. Napíšte Bobovi, ako sa pesnička \
volá. Takto znie preložený text:\n\n```I guess they won't meet  Two suitable poles  Sometimes we collide  It often hurts  If you want to forget then forget  \
Not coming possible If you want to forget then forget One day it will smother you```", makeLiteralChecker("lavíny"))
const pesnicka_3 = new Task("Pesnička 3", "Bob sa pomýlil a hodil **slovenskú** pesničku, na ktorú veľmi rád tancuje, do google prekladaču a tá sa \
preložila do angličtiny. Bob ale zabudol, ako sa volala. Rád by si znova zatancoval. Napíšte Bobovi, ako sa pesnička volá. Nepíšte interpreta, iba \
názov. Takto znie preložený text:\n\n```forest fairies dance  Lead every time  Let's play what to give  Here he is left  Also for people  Friends come the \
day  Mine like this in the fall  We'll come back here for the song.```", makeLiteralChecker("dobrý večer priatelia"))
const pesnicka_4 = new Task("Pesnička 4", "Bob sa pomýlil a hodil **slovenskú** pesničku, ktorá mu pripomína jeho detskobotské časy do google prekladaču \
a tá sa preložila do angličtiny. Bob ale zabudol, ako sa volala. Rád by si ju ale vypočul a zaspomínal si. Napíšte Bobovi, ako sa pesnička volá. \
Nepíšte interpreta, iba názov. Takto znie preložený text:\n\n```is no longer in the shower  and the pity available, it was washed away with water \
should not think so stupid  that is the shower of the phone what it calls```", makeLiteralChecker("dve malé blchy"))
const nahravka = new Task("Nahrávka", "Vytvor nahrávku, v ktorej sa čo najviac pokúsiš priblížiť frekvencii 512 Hz. Nahrávka nesmie byť upravovaná \
a zvuk nesmie vychádzať z reproduktora ani byť generovaný elektronikou. Nahrávku pošli Bobovi.", attachmentChecker)
const ta_otazka = new Task("Tá otázka", "Modrá alebo červená? To je otázka. Tak ju odpovedzte. Mainstreamová odpoveď získa body. Submitnite \
Bobovi `modrá` alebo `červená`", makeOneOfChecker(["modra", "cervena"], "Očakávam `modrá` alebo `červená`."))
const priekopnik = new Task("Priekopník", "Aké bolo prvé video nahrané na youtube? Submitnite jeho link v tvare <https://www.youtube.com/watch?v=ID>.",
  makeLiteralChecker("https://www.youtube.com/watch?v=jNQXAC9IVRw"))
const hrame_sa = new Task("Hráme sa", "V karanténe sa iste radi zahráte aj nejakú počítačovú hru. Tak si zahrajte a splňte 3 z týchto šiestich herných \
questov a dôkaz (screenshotom) o tom pošlite bobovi vo forme zipka (alebo rarka či podobného). Tieto hry sú nasledovné:\n    1) Získaj hut v \
<http://adarkroom.doublespeakgames.com/>\n    2) V hre cookie clicker (<https://orteil.dashnet.org/cookieclicker/>) získaj javascript konzolu\n    3) \
Vyhraj solitude na <http://www.windows93.net/>\n    4) Vyrieš puzzle <https://www.jigsawplanet.com/?rc=play&pid=269ed5f0b980>\n    5) Vytvor veľký nápis OŠP \
v minecrafte\n    6) Vyhraj zelovoc za troma stenami (ten z Pary: <https://github.com/sobkulir/para/releases/tag/v0.0.1>)", attachmentChecker)
const koala = new Task("Koala", "Vytvorte koalu (tu <https://www.koalastothemax.com/>). A pekne dopodrobna. Screenshot pošlite Bobovi.", attachmentChecker)
const filmy = new Task("Filmy", "Vypočujte si nahrávku z 5. filmov. Aké filmy to boli, uveď v správnom poradí. Pozor táto úloha **nie je** automaticky \
opravovaná Bobom (máte iba 1 submit). Ak neuhádtnete všetky filmy, dostanete čiastočné body :) <https://bit.ly/3ah6aE3>", nonEmptyChecker)

const project_euler_1 = new Task("Project Euler 1", "Farmár Emo zasadil v jeseni cesnak. Pamätá si iba, že počet strúčikov po delení každým z čísel \
od 2 po 10 vrátane, teda čísel z intervalu [2, 10], dával zvyšok 1, a po delení 11 dával zvyšok 0. Koľko najmenej strúčikov Emo mohol zasadiť?", makeLiteralChecker("25201"))
const project_euler_2 = new Task("Project Euler 2", "Prefix na rohu internetu našiel lístok s nápisom: `XY + XY = YZZ; heslo je XYZ`. \
Domnieva sa, že `X`, `Y`, aj `Z` sú nenulové cifry. Viete mu poradiť, aké je heslo?", makeLiteralChecker("612"))
const sifra_1 = new Task("Šifra 1", "*Nasledujúci obrázok obsahuje šifru. Riešením je 1 vlastné podstatné meno. Vyriešte ju a pošlite toto riešenie \
Bobovi. V tomto texte sa nenachádza šifra.* <https://bit.ly/2VioYyw>", makeLiteralChecker("Horacius"))
const sifra_2 = new Task("Šifra 2", "*Nasledujúci obrázok obsahuje šifru. Riešením je 1 podstatné meno (v slovenčine). Vyriešte ju a pošlite \
toto riešenie Bobovi.  V tomto texte sa nenachádza šifra.* <https://bit.ly/3cm0FW2>", makeLiteralChecker("let"))
const vypomoc = new Task("Výpomoc", "Rodičia pri homeoffice, homeschoolingu a všetkom ostatnom určite potrebujú vašu pomoc. Tak sa ich spýtajte \
s čím práve chcú pomôcť, pomôžte im, a zdokumentujte to. Pošlite Bobovi napr. zazipované fotky.", attachmentChecker)
const nespoznana = new Task("Nespoznaná", "Možno ste doma našli nejaký záhadný a pofidérny predmet, ktorý určite nikto nepozná. Tak ho odfoťte \
a pošlite ho Bobovi. Po súťaži budete môcť v špeciálnom kanáli `co-to-je` spoznávať predmety ostatných tímov za body.", attachmentChecker)
const zakladna_skola = new Task("Základná škola", "Určite viete počítať od nula do päťdesiat. Tak napočítajte Bobovi. V **súkromnej správe** \
napíšte Bobovi `!bob:pocitame`. Keď dopočítate, dostanete od Boba heslo, ktoré submitnete.", makeLiteralChecker("6174"))
const nepopularne_vyhrava = new Task("Nepopulárne vyhráva", "Myslite si celé číslo od 0 do 31 a napíšte nám ho. Unikátne čísla naprieč \
tímami získajú body.", makeOneOfChecker(Array.from({ length: 32 }, (_, i) => i).map(x => x.toString()), "Očakávam číslo od 0 do 31."))
const pchaj_sa = new Task("Pchaj sa!", "Vezmi si 1 papier veľkosti A4 a len strihaním (bez lepenia a iného upravovania) v ňom urob taký otvor, \
cez ktorý sa dokážeš prepchať. Proces prepchávania zdokumentuj fotkou a pošli ju Bobovi.", attachmentChecker)
const sifra_3 = new Task("Šifra 3", "*Nasledujúci obrázok obsahuje šifru. Riešením je 1 podstatné meno (v slovenčine). \
Vyriešte ju a pošlite toto riešenie Bobovi. V tomto texte sa nenachádza šifra.* <https://bit.ly/2XJuJHd>", makeLiteralChecker("lano"))
const den_pred_lockdownom = new Task("Deň pred lockdownom", "Spočítaj ľudí na fotke. Svoj tip pošli Bobovi. Pozor, submitnúť váš tip môžete \
iba raz! <https://bit.ly/3ct1Seq>", nonEmptyChecker)
const je_mi_horuco = new Task("Je mi horúco", "Kde na svete je práve najvyššia teplota (teplota vzduchu nad povrchom zeme). \
Pošlite názov miesta aj aká vysoká teplota to je.", nonEmptyChecker)
const vy_ste_to_chceli = new Task("Vy ste to chceli", "Vymysleli ste si quest. Tak si ho splňte. A dôkaz doručte Bobovi.", nonEmptyChecker)
const a_zas_sa_hras = new Task("A zas sa hráš", "A je to tu zase. Z týchto šiestich achievmentov získajte tri a zazipované nám pošlite \
dokumentačné screenshoty:\n    1) Prejdi 500m v google chrome dinosaurovi\n    2) Ziskaj 512 na hre 2048\n    3) Daj skóre 200 v tejto \
hre: <https://www.gamee.com/game/oFfW2omiW> \n    4) Dostaň sa do top 50 v slither.io \n    5) Prejdi stredné míny \n    6) Prejdi 6 metrov v \
tejto hre <http://www.foddy.net/Athletics.html>", attachmentChecker)
const kartas = new Task("Kartáš", "Máte balíček kariet? Výborne. Spravte z nich čo najväčšiu vežu a zdokumentujte.", attachmentChecker)
const lalalalala = new Task("Lalalalala", "Ako dlho viete rozprávať bez nádychu? Ukážte nám to. Napíšte nejakému vedúcemu \
a na videohovore nám svoje skilly ukážete. Potom submitnite výsledok v tvare <nick veduceho> <pocet sekund>.", nonEmptyChecker)
const malopocetna = new Task("Malopočetná", "Bob má rád otázky s málo odpoveďami. Ale nejaké mať musí. Nájdite frázu, \
ktorá v Google vyhľadávaní dá medzi 1 a 5 výsledkami a pošlite nám ju.", nonEmptyChecker)
const selfie = new Task("Selfie", "Sprav si selfie! A s čím by bolo lepšie spraviť si selfie ako s OK submitom na úlohu. Vyriešte \
úlohu <https://people.ksp.sk/~acm/problem.php?problem=gcd> a pošlite Bobovi selfie s OK submitom. Ak ste úlohu mali vyriešenú už pred \
hrou, riešte <https://people.ksp.sk/~acm/problem.php?problem=cwtree>. Ak ste mali pred hrou obe, vyberte si nevyriešenú úlohu podľa \
uváženia :))", attachmentChecker)
const nemozne_farby = new Task("Nemožné farby", "Vieš o tom, že existujú farby, ktoré nedokážeme vidieť našim okom? Uveď bobovi \
príklad takejto farby.", nonEmptyChecker)
const kamziky = new Task("kamziky", "Počuli ste už o binárnych kamzíkoch? Sú to kamzíky, ktoré môžu byť iba v dvoch stavoch. Keď sa \
vedľa seba postavia 3, môžu tak spraviť čísla 0-7 (2^3 = 8). Vymyslite kreatívny spôsob reprezentovania dvoch stavov, a v submite \
pošlite jeho stručný popis a fotku ako ukazujete číslo 5. (Môžete poslať napr. screenshot zo Skype callu, alebo zlepené \
  fotky)", attachmentAndTextChecker)
const papieropas = new Task("Papieropás", "Vezmi si 1 papier veľkosti A4 a strihaním z neho vytvor najdlhší súvislý úsek aký \
zvládneš. Odfoť ho tak, aby bolo vidno jeho rozmer a k fotke pripíš dĺžku. Naša vedúca Sabinka vystrihla takmer 20 metrov, \
prekonáš ju?", attachmentChecker)
const strajk = new Task("Štrajk", "Bob sa rozhodol štrajkovať. Nikto ale nevie, čo vlastne štrajkom chce povedať. Zistite to: \
v **súkromnej správe** mu napíšte `!bob:strajkuj`. Výsledný text submitnite.", makeLiteralChecker("Testing shows the presence not the absence of bugs."))
const streetview = new Task("Steetview", "Bob rád navštevuje slovenské obce. Teraz ale nemôze výjsť ani z vlastnej dediny. Choďte as s ním teda prejsť aspoň virtuálne \
v **súkromnej správe** pomocou `!bob:streetview`. Submitni výsledok v tvare '<obec> <odpoved>' **poslednej vyriešenej dediny**.", makeLiteralChecker("Poproč biela"))

const TASKS = [
  nahravka, ta_otazka, priekopnik, bob_krici_1, photoshop_spolu, pesnicka_3, knihomol, project_euler_1, nezodpovedana, sifra_1, hybrid, hrame_sa,
  kolacova, koala, filmy, vypomoc, pesnicka_2, anketa, miro_jaros_1, bob_krici_2, pite_prvocislo, nespoznana, project_euler_2, zverinec, pesnicka_4,
  miro_jaros_2, zrucnost, sifra_2, korespondencne_seminare, strajk, nepopularne_vyhrava, neporiadok, pchaj_sa, sifra_3, streetview, den_pred_lockdownom,
  connection_lost, je_mi_horuco, basnicka, logo, i_like_trains, miro_jaros_3, kniznica, pesnicka_1, zostan_zdravy, a_zas_sa_hras, kartas, gulocka,
  lalalalala, lavosuffle, malopocetna, umelecky_talent, selfie, nemozne_farby, narodeniny, kvetiny, kamziky, papieropas, spusti_to, cukrovy_zub]

module.exports = { TASKS, Answer }