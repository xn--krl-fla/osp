const { Router, COMMANDS } = require('./router')
const db = require('./db')

// Returns Array<string>
function createMentions(id) {
  return [`<@${id}>`, `<@!${id}>`]
}

// Returns bool
function hasInitialMention(str, id) {
  return createMentions(id).some(mention => str.startsWith(mention))
}

// Returns stripped `str`.
function stripInitialMention(str, id) {
  const mentions = createMentions(id)
  for (const m of mentions) {
    if (str.startsWith(m)) {
      return str.slice(m.length)
    }
  }
  throw new Exception(`No mention in ${str}.`)
}

class Vikendoffka {
  // `router: Router`
  // `bobId: number`
  constructor(router, bobId) {
    this.router = router
    this.bobId = bobId
  }

  static buildBasic(bobId) {
    return new Vikendoffka(new Router(COMMANDS), bobId)
  }

  // Handles a single `msg`. The return value specifies whether `msg` was from one of the
  // team channels.
  async handleMsg(msg, logger) {
    try {
      const state = await db.fetchState()
      if (this.isMsgFromTeamChannel(msg, state)) {
        await this.tryRoute(msg, state)
        return true
      } else {
        return false
      }
    } catch (error) {
      console.error(error.stack)
      await msg.channel.send('Zrútil som sa :/')
      return true
    }
  }

  // `msg: Message`
  // `state: {teamMap: Map<teamChannelName: string, Team>}` a MongoDB document.
  isMsgFromTeamChannel(msg, state) {
    return state.teamMap.has(msg.channel.name)
  }

  // `msg: Message`
  // `state: {teamMap: Map<teamChannelName: string, Team>}` a MongoDB document.
  async tryRoute(msg, state) {
    let body = msg.content.trimStart()
    if (hasInitialMention(body, this.bobId)) {
      body = stripInitialMention(body, this.bobId)
      await this.router.route(body, msg, state)
    }
  }
}

module.exports = { Vikendoffka }
