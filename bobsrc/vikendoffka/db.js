const mongoose = require('mongoose');
const { Team, GameState } = require('./team')
const { Answer } = require('./task')
const { TEAMS } = require('./constants')

const url = `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@localhost:27017/`;
mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }).catch(reason => { console.log(`Failed to connect to mongoDB. You have to start it with 'docker-compose build&&docker-compose up'. ${reason}`) });

// Cheatsheet:
// await State.find().deleteMany() -- deletes all records from model State
// await State.find() -- returns all records from model State
// await model.forceSave() -- saves the whole document (forceSave is defined below)

// Schema hacks.
const answerSchema = new mongoose.Schema({
  date: Date,
  body: String,
  attachments: [String]
})
answerSchema.loadClass(Answer)

const gameStateSchema = new mongoose.Schema({
  startDate: Date,
  taskProgress: [answerSchema]
})
gameStateSchema.loadClass(GameState)

const teamSchema = new mongoose.Schema({
  channelName: String,
  fullName: String,
  gameState: gameStateSchema
})
teamSchema.loadClass(Team)

const stateSchema = new mongoose.Schema({
  teamMap: {
    type: Map,
    of: teamSchema
  }
})

const State = mongoose.model('State', stateSchema)

// The document is always saved as a whole, using forceSave().
State.prototype.forceSave = async function () {
  this.markModified('teamMap')
  await this.save()
}

// All teams will be defined here.
function _getDefaultState() {
  if (process.env.DEBUG === "true") {
    return new State({
      teamMap: new Map([
        ['testtest', new Team('testtest', "Bot test", undefined)]
      ])
    })
  } else {
    return new State({
      teamMap: new Map(TEAMS.map(x => [`tím_${x.channelName}`, new Team(`tím_${x.channelName}`, x.name, undefined)]))
    })
  }
}
// Fetches state from DB or uses `defaultState` if DB has no records.
async function fetchState() {
  const records = await State.find()
  return (records.length === 0) ? _getDefaultState() : records[0]
}

module.exports = { fetchState }
