const { GameState } = require('./team')
const { Answer, TASKS } = require('./task')
const constants = require('./constants')
const { getRandElem } = require("../utilities")

class Router {
  // `commands: Array<Command>` contains commands that should be routed.
  constructor(commands) {
    this.commands = commands
  }

  // `body: string` message content
  // `origMsg: Message` original message
  // `state: State` from DB
  async route(body, origMsg, state) {
    body = body.trimStart()
    for (const cmd of this.commands) {
      if (body.startsWith(cmd.name)) {
        return cmd.handle(body.slice(cmd.name.length), origMsg, state)
      }
    }
    const cmdName = body.split(' ')[0]
    await origMsg.channel.send(`Nepoznám príkaz: "${cmdName ? cmdName : '<prazdny prikaz>'}"`)
  }
}

class Command {
  // `name` in the chat that invokes the command
  // `handle(args: Array<string>, origMsg: Message, state: State): void` callback for the command 
  constructor(name, handle) {
    this.name = name
    this.handle = handle
  }
}

// --------------------Commands-----------------------------
async function spustihru(_body, origMsg, state) {
  const team = state.teamMap.get(origMsg.channel.name)
  if (team.gameState) {
    return origMsg.channel.send("**Nespustil som hru**: Hra buď práve beží alebo už prebehla.")
  }

  const startDate = new Date()
  // if (process.env.DEBUG !== "true" && (startDate < 9 || startDate >= 21)) {
  //   return origMsg.channel.send("**Nespustil som hru**: Začať môžete iba medzi 9 a 21 :)")
  // }

  team.gameState = new GameState(startDate)
  await state.forceSave()
  return origMsg.channel.send(
    `**Hra je spustená!**
Môžete riešiť do ${team.gameState.getEndDate().toLocaleString()}. Teraz môžete napríklad zobraziť úlohy pomocou \`ulohy\`, \
a prikazy pomocou \`help\`. Veľa šťastia :four_leaf_clover:`)
}

// On success returns: {
//  taskId: Number,
//  newBody: string
// }
// On failure returns: false
// 
// `body: string` body of the message
// `async print(str: string)` print function
async function _validateTaskIdFormat(body, print) {
  const trimmedBody = body.trimStart()
  const cisloStr = trimmedBody.split(/\s/)[0]
  if (cisloStr === "") {
    await print("Submit musí začínať číslom úlohy.")
    return false
  }
  const cisloInt = parseInt(cisloStr)
  if (!/^\d+$/.test(cisloStr) || cisloInt < 0) {
    await print(`Ćíslo úlohy musí byť nezáporné celé číslo, ale dostal som ${cisloStr} :(`)
    return false
  }
  return {
    taskId: cisloInt,
    newBody: trimmedBody.slice(cisloStr.length).trimStart()
  }
}

async function submit(body, origMsg, state) {
  const team = state.teamMap.get(origMsg.channel.name)
  if (!team.isInGame()) {
    return origMsg.channel.send("**Nemôžem pre vás submitnúť úlohu**: Nie ste v hre. Buď ste ešte nezačali alebo vám už vypršal čas.")
  }

  const taskIdOk = await _validateTaskIdFormat(body, (str) => origMsg.channel.send(str))
  if (!taskIdOk) return

  const { taskId, newBody } = taskIdOk
  if (!team.gameState.isTaskIdVisible(taskId)) {
    return origMsg.channel.send(`Nemôžete submitovať úlohu číslo ${taskId}. Všetky aktuálne dostupné úlohy nájdete príkazom "ulohy".`)
  }

  if (await TASKS[taskId].checker(newBody, origMsg)) {
    team.gameState.taskProgress[taskId] = new Answer(new Date(), newBody, origMsg.attachments.map(x => x.url))
    await state.forceSave()
  } else {
    await origMsg.channel.send(getRandElem(constants.FACEPALMS))
  }
}

async function ulohy(_body, origMsg, state) {
  const team = state.teamMap.get(origMsg.channel.name)
  let out = ''
  if (!team.gameState) {
    return origMsg.channel.send(`Zatiaľ ste **nespustili hru**. To spravíte príkazom "spustihru". Vtedy sa vám začne odpočítavať čas ${constants.GAME_TIME_H} hodiny.`)
  }

  const endDate = team.gameState.getEndDate()
  if ((new Date()) < endDate) {
    out += `Máte čas do ${endDate.toLocaleString()} :timer:\n`
  } else {
    out += `Hra skončila o ${endDate.toLocaleString()} :timer:\n`
  }

  const taskIds = team.gameState.getVisibleTaskIds()
  if (taskIds.length === 0) {
    out += "Vyriešili ste **všetky úlohy**! :P"
  } else {
    out += taskIds.map(id =>
      `**${id}. ${TASKS[id].name}**: ${TASKS[id].description}\n`).join('')
  }

  return origMsg.channel.send(out)
}

async function list(body, origMsg, state) {
  const bounds = body.trim().split(' ').map(x => parseInt(x))
  bounds[1] = Math.min(bounds[1], 10)

  const idxs = Array.from({ length: 1000 }, (_, i) => i)
  let out = ''
  out += idxs.slice(bounds[0], Math.min(bounds[0] + bounds[1], TASKS.length)).map(id =>
    `**${id}. ${TASKS[id].name}**: ${TASKS[id].description}\n`).join('')

  return origMsg.channel.send(out)
}

async function ulohy(_body, origMsg, state) {
  const team = state.teamMap.get(origMsg.channel.name)
  let timeText = ''
  if (!team.gameState) {
    return origMsg.channel.send(`Zatiaľ ste **nespustili hru**. To spravíte príkazom "spustihru". Vtedy sa vám začne odpočítavať čas ${constants.GAME_TIME_H} hodiny.`)
  }

  const endDate = team.gameState.getEndDate()
  if ((new Date()) < endDate) {
    timeText += `Máte čas do ${endDate.toLocaleString()} :timer:\n`
  } else {
    timeText += `Hra skončila o ${endDate.toLocaleString()} :timer:\n`
  }

  const taskIds = team.gameState.getVisibleTaskIds()
  if (taskIds.length === 0) {
    return origMsg.channel.send(timeText + "Vyriešili ste **všetky úlohy**! :P")
  } 

  const taskDescs = taskIds.map(id =>
      `**${id}. ${TASKS[id].name}**: ${TASKS[id].description}`)

  if ((timeText + taskDescs.join('\n')).length < 1950) {
    return origMsg.channel.send(timeText + taskDescs.join('\n')) 
  }

  return Promise.all([
    origMsg.channel.send(timeText),
    taskDescs.map(desc => origMsg.channel.send(desc))
  ].flat())
}

async function help(body, origMsg, state) {
  return origMsg.channel.send(
    `\`spustihru\` -- medzi 9 a 21 spustí hru
\`submit <cislo_ulohy> <text>\` -- submit úlohy
\`ulohy\` -- vypíše zoznam dostupných úloh
\`casomiera\` -- vypíše čas do konca
\`info\` -- vypíše informácie o hre
\`help\` -- výpis príkazov :)`
  )
}

async function info(body, origMsg, state) {
  return origMsg.channel.send(`Prezradím vám *tajomstvo*: Súťaž si budete môcť pustiť od 9:00 pomocou príkazu \`spustihru\`.
Následne dostanete prvé úlohy, ktoré budete môcť plniť. Keď ich budete mať, odovzdáte úlohu pomocou príkazu \`submit <cislo_ulohy> <text>\`. 
K príkazu sa dá pripojiť aj príloha - text/obrázok/zip archív - k niektorým úlohám to bude treba. Po úspešnom odovzdaní vám odovzdanú úlohu vymením za novú. 
Niektoré veci mi budú musieť pomôcť vyhodnotiť vedúci, čiže skutočné skóre sa dozviete až po skončení súťaže. ||@(hihi)|| 
Počas súťaže si tiež môžete zobraziť zostávajúci čas pomocou príkazu \`casomiera\`, vypísať dostupné úlohy pomocou príkazu \`ulohy\` alebo vypísať dostupné príkazy pomocou príkazu \`help\`. 

Ak by ste mali nejakú otázku alebo problém, taggnite "starostovia a starostky" v správe v tomto kanáli, napríklad (bez medzery za @): @ starostovia a starostky Nefunguje nám úloha Mrkvička, lebo ...

P.S.: **Pred každým príkazom ma musíte označíť, aby som vedel že ma niekto potrebuje.**
Príklad odovzdania úlohy 3 s riešením "koberec": <@691031426430271540> submit 3 koberec
Toto môžete skúsiť už hneď teraz: <@691031426430271540> help`)
}

async function casomiera(body, origMsg, state) {
  const team = state.teamMap.get(origMsg.channel.name)
  if (!team.isInGame()) {
    return origMsg.channel.send("**Nie ste v hre**: Nemôžem vám preto povedať, koľko zostáva do jej konca :face_with_monocle:")
  }
  const minToEnd = Math.ceil((team.gameState.getEndDate() - (new Date())) / (1000 * 60))
  return origMsg.channel.send(`Do konca vám zostáva **${minToEnd} minut** :timer:`)
}

async function teams(body, origMsg, state) {
  const all = Array.from(state.teamMap.values())
  const preinpost = [all.filter(t => !t.gameState), all.filter(t => t.isInGame()), all.filter(t => t.hasFinished())]
  const preinpostStr = preinpost.map(x => x.map(t => `\`${t.channelName}\``).join(', '))
  await origMsg.channel.send(`**Pregame:** ${preinpostStr[0]}
**Ingame:** ${preinpostStr[1]}
**Postgame:** ${preinpostStr[2]}`)
}

// An array of all the commands.
const COMMANDS = [
  new Command('echo', async (body, origMsg, _state) => {
    if (body !== '') {
      return origMsg.channel.send(body)
    }
  }),
  new Command('spustihru', spustihru),
  new Command('submit', submit),
  new Command('ulohy', ulohy),
  new Command('help', help),
  new Command('info', info),
  new Command('casomiera', casomiera),
  new Command('&teams', teams),
  new Command('&list', list),
]

module.exports = { Router, Command, COMMANDS }
