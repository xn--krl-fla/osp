const GAME_TIME_H = 2
const GAME_TIME_MS = GAME_TIME_H * 60 * 60 * 1000
const VISIBLE_TASKS_COUNT = 5

const POSITIVE_FEEDBACK = [
  "Áno, to je **správne**!",
  "**Super**! Máte ďalšiu úlohu",
  "**Výborne**, Bob je šťastný :partying_face: Zobrazte si novú úlohu.",
  "Práve ste **správne** submitli úlohu!",
  "**Jupííí**! Zobrazte si novú úlohu :)"
]
const NEGATIVE_FEEDBACK = [
  "Nie, to **nie je** správne :/",
  "Hmmm, Bob **nesúhlasí**, skúste to znovu.",
  "Odpoveď je **nesprávna** :(",
  "**No no no no no**. Ďalší pokus prosím.",
  "Tady vám pšenka **nepokvetla**."
]
const ACK_FEEDBACK = [
  "**Podarilo sa** vám submitnúť úlohu :))",
  "**Oukej!** Môžete ísť na ďalšiu úlohu.",
  "Super, zapisujem si to. Dostali ste ďalšiu úlohu :partying_face:",
  "Pokračujeme v krasojízde! Submitli ste úlohu a dostávate ďalšiu :)"
]
const FACEPALMS = [
  ':woman_facepalming:', ':man_facepalming_tone5:', ':man_facepalming_tone4:', ':man_facepalming_tone3:',
  ':man_facepalming_tone3:', ':man_facepalming_tone2:', ':man_facepalming_tone1:', ':man_facepalming:',
  ':person_facepalming_tone5:', ':person_facepalming_tone4:', ':person_facepalming_tone3:', ':person_facepalming_tone2:',
  ':person_facepalming_tone1:', ':person_facepalming:', ':woman_facepalming_tone5:'
]

const TEAMS = [
 {id:1, channelName:"nemal-som-čas-na-vymýšľanie", name: 'Nemal som čas na vymýšľanie', members: ['paradox4324', 'Jajopi', 'placeholder']},
  {id:2, channelName:"tatryio", name: 'Tatryio', members: ['Mylosh#6259', 'Timea#6134', 'Rôbkä']},
  {id:3, channelName:"beranka", name: 'Beranka', members: ['TomasNgu', 'emmab', 'Ja']},
  {id:4, channelName:"kvietočky", name: 'Kvietočky', members: ['Marianka', 'knedlicka', 'sammko']},
  {id:5, channelName:"antiužovka", name: 'AntiUžovka', members: ['MataxePlay#8006', 'NotImportant#0262', 'GP333#2430']},
  {id:6, channelName:"b̵̃̅r̶͗͗ǘ̸̓h̷͗͛", name: 'B̵̃̅r̶͗͗ǘ̸̓h̷͗͛', members: ['Babička#0894', 'Karamelka42#5767', 'fAKEjurajmozax#0980']},
  {id:7, channelName:"externí-riešitelia", name: 'Externí riešitelia', members: ['Karlord19', 'wikiker#4444', 'zdeněk#5721']},
  {id:8, channelName:"spielverderber", name: 'Spielverderber', members: ['MvKal#6472', 'MatusH#6703', 'Jakub Drobný#0824']},
  {id:9, channelName:"0w0-what-is-this", name: '0w0, what is this', members: ['mhavos#8227', 'dtep#0420', 'Viktor#8904']},
  {id:10, channelName:"mocipáni", name: 'Mocipáni', members: ['bodonkles#6221', 'Peťo Kochelka#2799', 'Klimi#5885']},
  {id:11, channelName:"ingzvs-zväz-všetkých-sprostých", name: 'Ing.ZVS (zväz všetkých sprostých)', members: ['utir', 'Migelvojto', 'sekac']},
  {id:12, channelName:"elektraren123", name: 'ELEKTRAREN123', members: ['adam', 'eLiška', 'rajsky_peter']},
  {id:13, channelName:"the-sleeping-knights", name: 'The Sleeping Knights', members: ['Tobyyy#8720', 'Thunderhead#9336', 'Cirrus#6112']},
  {id:14, channelName:"názov-registrovaného-tímu", name: 'názov registrovaného tímu', members: ['emmika', 'kristina', 'whinygold']},
  {id:15, channelName:"uncaught-syntaxerror-missing-yeet-after-argument-list", name: 'Uncaught SyntaxError: missing ) after argument list', members: ['Martin#6469', 'Ericek42', 'yes.']},
  {id:16, channelName:"štvorica-troch-šamanov", name: 'Štvorica troch Šamanov', members: ['Dz4vo#0203', 'Lazy#7423', 'Saplington#3509']},
  {id:17, channelName:"artful-aardvark", name: 'Artful Aardvark', members: ['Cubean42#4828', 'Matus240', 'Kika#2101']},
  {id:18, channelName:"bionic-beaver", name: 'Bionic Beaver', members: ['Jožura', 'branislav', 'Level3301']},
  {id:19, channelName:"cosmic-cuttlefish", name: 'Cosmic Cuttlefish', members: ['Fjolt#2707', 'jac08h', 'M4JCI#5214']},
  {id:20, channelName:"disco-dingo", name: 'Disco Dingo', members: ['gaspar', 'Vector_SVK#0115', 'Tímea Hoangová']},
  {id:21, channelName:"eoan-ermine", name: 'Eoan Ermine', members: ['mrazbb#6439', 'MATOM_57']},
  {id:22, channelName:"radosť", name: 'radosť', members: ['alicka', 'DaniMe2czky', 'Uršuľa']},
  {id:23, channelName:"veni-vidi-vici", name: 'Veni Vidi Vici', members: ['matolin#6614', 'Gery#4069', 'Matolin#5032']},
  {id:25, channelName:"nsp", name: 'nsp', members: ['Naďa#2666', 'stolo93#1911', 'Trisha#6126']},
  {id:26, channelName:"chickem-games", name: 'Chickem games', members: ['mitokk', 'ShiftiikGames', 'Hedgie']},
  {id:27, channelName:"koberec8", name: 'Koberce', members: ['sobkulir', 'kral']},
  {id:28, channelName:"focal-fossa", name: 'Focal Fossa', members: ['žeriav', 'Tellegar', "Arduman4"]},
]

module.exports = { GAME_TIME_H, GAME_TIME_MS, VISIBLE_TASKS_COUNT, POSITIVE_FEEDBACK, NEGATIVE_FEEDBACK, ACK_FEEDBACK, FACEPALMS, TEAMS }