const { TASKS } = require('./task')
const constants = require('./constants')

class Team {
  // `channelName: string` is Discord channel name of the team
  // `fullName: string` is team full name
  // `gameState: GameState` team's GameState object
  constructor(channelName, fullName, gameState) {
    this.channelName = channelName
    this.fullName = fullName
    this.gameState = gameState
  }

  isInGame() {
    if (this.gameState) {
      const now = new Date()
      const start = this.gameState.startDate
      const end = this.gameState.getEndDate()
      return (start < now && now < end)
    } else {
      return false
    }
  }

  hasFinished() {
    return (this.gameState && (new Date()) > this.gameState.getEndDate())
  }
}

class GameState {
  // `startDate: Date` is the time when team started the game 
  // `taskProgress: List<Answer | null>` is task[i] is non-null if i-th task was solved
  constructor(startDate) {
    this.startDate = startDate
    this.taskProgress = new Array(TASKS.length).fill(null);
  }

  // Returns Date.
  getEndDate() {
    return new Date(this.startDate.getTime() + constants.GAME_TIME_MS)
  }

  // Returns `Array<number>` of first (up to) 5 unsolved problem ids.
  getVisibleTaskIds() {
    return this.taskProgress.map((x, idx) => (x === null) ? idx : -1)
      .filter(x => x !== -1).slice(0, constants.VISIBLE_TASKS_COUNT)
  }

  isTaskIdVisible(id) {
    return this.getVisibleTaskIds().includes(id)
  }
}

module.exports = { GameState, Team }
