const englishWords = require('an-array-of-english-words')
const STRAJK_SOLUTION = "Testing shows the presence not the absence of bugs."

// Creates an arbitrary zwarte-like story.
function createZwarte(invokeMsg, initialResponse, finalResponse, solution, checker) {
  const isCorrect = (msg) => msg.content === solution && !msg.author.bot

  return [{
    validator: msg => msg.content === invokeMsg && !msg.author.bot,
    callback: (channel, _) => channel.send(initialResponse)
  },
  {
    validator: msg => !msg.author.bot, // Take every input
    skipWhen: isCorrect,
    callback: checker
  },
  {
    validator: isCorrect,
    callback: (channel, _) => channel.send(finalResponse)
  },
  {
    validator: (msg) => !msg.author.bot,
    callback: () => false
  }]
}

// Validates `str` to be an integer.
const isZwarteInteger = (channel, str) => {
  if (str.length > 50) {
    channel.send("Takým dlhým správam nerozumiem :(")
    return false
  } else if (!/^\d+$/.test(str)) {
    channel.send("Hmm, rozumiem iba reťazcom z cifier.")
    return false
  } else {
    return true
  }
}

// Zwarte1 adds digit's value to its position in the string.
const zwarteChecker1 = (channel, msg) => {
  const str = msg.content
  if (!isZwarteInteger(channel, str)) {
    return true // This is a magic true.
  }
  return channel.send(
    str.split("").map((c, idx) => (parseInt(c) + idx) % 10).join(""))
}


// Zwarte2 maps numbers to count of consecutive digits of the same kind and
// the digit itself.
const zwarteChecker2 = (channel, msg) => {
  const str = msg.content
  if (!isZwarteInteger(channel, str)) {
    return true // This is a magic true.
  }

  let lastC = str[0]
  let count = 1
  let out = ""
  for (let i = 1; i < str.length; i++) {
    if (str[i] == lastC) {
      ++count
    } else {
      out += `${count}${lastC}`
      count = 1
      lastC = str[i]
    }
  }
  out += `${count}${lastC}`
  return channel.send(out)
}

// Streetview
function createStreetviewStep(solution, textAfter) {
  const isCorrect = msg => msg.content.toLowerCase() === solution && !msg.author.bot
  return [
    {
      validator: (msg) => !msg.author.bot,
      skipWhen: isCorrect,
      callback: (channel, _) => channel.send("Niee to nie je správne :(")
    },
    {
      validator: isCorrect,
      callback: (channel, _) => channel.send(textAfter)
    },
  ]
}

// Strajk
const strajkChecker = (channel, msg) => {
  const str = msg.content
  if (str === STRAJK_SOLUTION) {
    return true
  }

  vypis = (out) => channel.send(out)

  if (str === "") {
    return vypis("**Štrajkujem,** pretože veta nemôže byť prázdna!")
  }
  if (str[str.length - 1] !== ".") {
    return vypis("**Štrajkujem,** pretože veta nekončí bodkou!")
  }

  const words = str.substring(0, str.length - 1).toLowerCase().split(' ')
  if (words.length != 9) {
    return vypis("**Štrajkujem,** pretože veta nemá presne 9 slov!")
  }
  if (str[0].toUpperCase() !== str[0]) {
    return vypis("**Štrajkujem,** pretože písmeno prvého slova nie je veľké!")
  }

  if (str.slice(1, str.length - 1).split(' ').some(w => w.split('').some(c => c.toUpperCase() === c))) {
    return vypis("**Štrajkujem,** pretože niektoré písmeno okrem prvého je veľké!")
  }

  if (words.some(w => !englishWords.includes(w))) {
    return vypis(`**Štrajkujem,** lebo nasledovné anglické slovo nepoznám: "${words.find(w => !englishWords.includes(w))}"`)
  }
  const order = words.map((w, idx) => [w, idx]).sort().map(x => x[1] + 1).join('')
  if (order !== "795842136" && order !== "795842163") {
    return vypis("**Štrajkujem,** lebo slova vety nemaju spravne poradie v abecede. \
Spravne poradie slov je 7958421(36). (Najskor v abecede musi byt siedme slovo, potom \
deviate, potom piate, … Tretie a šieste slovo musia byt rovnake.")
  }

  const solWords = STRAJK_SOLUTION.substring(0, STRAJK_SOLUTION.length - 1).toLowerCase().split(' ')
  let errors = ""
  for (let i = 0; i < words.length; ++i) {
    if (words[i] < solWords[i]) {
      errors += `\nSlovo na ${i + 1}. mieste má byť neskôr v abecednom poradí.`
    } else if (words[i] > solWords[i]) {
      errors += `\nSlovo na ${i + 1}. mieste má byť skôr v abecednom poradí.`
    }
  }

  if (errors !== "") {
    return vypis(`**Štrajkujem,** lebo niektoré slová nie sú správne:\n${errors}`)
  }
  return vypis("**Exception**: Niekde v mojom kóde sa stala chyba. Napíše prosím @sobkulir.")
}

const streetview = [
  {
    validator: msg => msg.content === "!bob:streetview" && !msg.author.bot,
    callback: (channel, _) => channel.send(
      `Ahoj :) Poznáš Google Street View? Minule som tam šiel na prechádzku s mojou \
capybarou, ale ušla mi. Pomôžeš mi ju nájsť?

V tejto úlohe je päť podúloh a bod dostanete za každú zriešenú. Každá podúloha \
sa týka nejakej slovenskej dediny, meno dediny dostanete za vyriešenú predchádzajúcu podúlohu. \
Na všetky úlohy stačí streetview, ak je napr. v dedine streetviewnutá len jedna ulica, ráta sá iba tá.\

Ak chcete začať hrať, napíšte: "Booob už hrajme!"`)
  },
  {
    validator: msg => msg.content === "Booob už hrajme!" && !msg.author.bot, // Take every input
    callback: (channel, _) => channel.send(
      `Začínate v obci **Regetovka**. Keď idete street viewom smerom do tejto obce, na jednom z domov \
je nápis, pravdepodobne označujúci obchod, ktorý sa tam nachádza. Odpoveď je tento nápis. \
Tak ako je napísaný, aj s diakritikou.`)
  },
  createStreetviewStep("miešaný tovar",
    `Áno, správne! Teraz pokračujeme do dediny **Havranec**. Pri vstupe do obce je tabuľa s nápisom \
"Vitajte v obci" (a nejaké pokračovanie). Zoberte si farbu pozadia, na ktorom je tento text napísaný. \
V dedine je dom ktorý má stenu danej farby. Pri dome je taký žltý cestný stĺpik, ktorý má na sebe číslo. \
Odpoveďou je toto číslo, tak ako je napísané na stĺpe.`),
  createStreetviewStep("4.0",
    `Áno, správne! Presuňme sa teraz do dediny **Ondavka**. Pri kostole je plot. Plot má niektoré latky \
biele a niektoré latky inej farby. Nájdite dom, ktorý má plus-mínus túto frabu. Odpoveď je číslo domu.`),
  createStreetviewStep("4",
    `Áno, správne! Poďme teraz do malebnej obce s názvom **Parihuzovce**. Keď idete z hranice územia \
vyznačenej na google mapách do tejto dediny narazíte na budovu netypického tvaru, ležiacu ešte pred dedinou. \
Ako idete ďalej do dediny, popri ceste sú nejaké autá. Koľko z áut má niektorú z farieb tej budovy? \
(Nemusí byť presne také farba, ale ak by ste farby označili rovnakým slovom, rátajú sa ako rovnaké).`),
  createStreetviewStep("2",
    `Áno, správne! Nakoniec poďme do dedinky s názvom **Poproč**. V dedinke je domov Sv. Anny. Mimo \
cesty je street view miesto, kde sa dá pozrieť. V čase fotenia tam kvitol strom. Aká je farba jeho kvetov? \
Farbu píšte v základnom tvare ženského rodu (bez skloňovania).`),
  createStreetviewStep("biela",
    `Áno, správne! Ďakujem, že si mi pomohol nájsť moju capybaru :))`)
].flat()

stories = {
  zwarte1:
    createZwarte(
      "!bob:jahoda",
      "Ahoj, zahráme sa hru! Skús ma prinútiť vypísať číslo 18181818 :)",
      "18181818 (vyhral si!)", "17957351", zwarteChecker1
    ),
  zwarte2:
    createZwarte(
      "!bob:klobuk",
      "Ahoj, z úst mi vkuse behajú nejaké čísla, ale ja by som chcel zakričať 123456 :)",
      "123456 (vyhral si!)", "244466666", zwarteChecker2
    ),
  strajk:
    createZwarte(
      "!bob:strajkuj",
      "Rozhodol som sa **štrajkovať**! Napíš mi nejakú vetu, možno prestanem.",
      "Správne! Doštrajkoval som :P", STRAJK_SOLUTION, strajkChecker
    ),
  streetview
}

module.exports = { stories };