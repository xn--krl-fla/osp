/*
* Funkcionality
* DONE: 1. Vítanie nových ľudí?
* DONE: 2. When idle channel, hype it
* DONE: 3. štatistiky
* DONE: 4. odpisovanie na príkaz
* TODO: discordapi unknownmessage error
* Next thing up: ako sa yargs prekrýva so stories?
* */

const {solutions, hints} = require("./cipherModule");

const {vyjadriSa} = require("./sentimentModule");

require('dotenv').config();
const Discord = require('discord.js');
const client = new Discord.Client();
const axiosModule = require("./axiosModule");
const statsModule = require("./statsModule");
const cleanupModule = require("./cleanupModule");
const {postHypersHandler, virtualExperiencesHandler, newUserHandler} = require("./socialModule");
const {nameToEmoji, emojiToName, reactToMsg} = require("./emojis");
const posts = require("./channelContent/posts.json");
const logger = require('./logger');
const imageWork = require('./imageWork');
let facts = require('./channelContent/facts');
const {processSubmit} = require("./contestWork");
const {runStories} = require("./storyModule");
const {contains} = require("./utilities");
const {getRanking, makeTable} = require("./contestWork");
const {mockText, popRandomElement, getRandomElement} = require("./utilities");
const {Vikendoffka} = require('./vikendoffka/vikendoffka');
const teamWorks = require("./teamWorks");


const {TEAMS} = require('./vikendoffka/constants');
const TOKEN = process.env.TOKEN;
const submitLinks = require("./vikendoffka/submitLinks");


const geolib = require('geolib');



let createOptionObjectWithFilesAttached = (msg) => ({
  files:msg.attachments.map(x => x.attachment)
});

client.login(TOKEN);

let vikendoffka;
let elevatedParser, plebsParser, cipherParser;
let botTestChannels;

client.on('ready', async (c) => {
  console.info(`Logged in as ${client.user.tag}!`);
  const generalChannels = client.channels.cache.filter(ch => ch.name === "general");    //yes i want unexpected type coercion! this is general channel
  console.info(`found ${generalChannels.array().map(x=>x).length} general channels`);
  // const {createTeam} = teamWorks(generalChannel,client);
  //TEAMS.map(team=> createTeam(team));

   botTestChannels = client.channels.cache.filter(ch => ch.name === 'bot-test');
  let usedChannels = client.channels.cache.filter(ch => ch.name in posts.channels);
  logger.info(usedChannels.map(ch => ch.name));

  //generalChannel.send(mockText(getRandomFact()));

  generalChannels.array().map( generalChannel => logger.info(`online v generali parenta ${generalChannel.parent.name} servera '${generalChannel}':${generalChannel.members.filter(m => m.presence.status === 'online').size}`));
  statsModule.startLogging(generalChannels.array());
  usedChannels.forEach(async channel => await postHypersHandler(channel, posts));   //init counters

  vikendoffka = Vikendoffka.buildBasic(client.user.id);
  let Yargs = require('yargs/yargs');
  plebsParser = new Yargs();
  plebsParser.scriptName("bob")
      .exitProcess(false)
      .recommendCommands()
      .command(['dajzazitok', 'dajzážitok'], 'ukáže zaujímavý submit z víkendoffky', {}, argv => {
        argv.didYargsRun = true;
        argv.msg.channel.send(getRandomElement(submitLinks))
      });

  elevatedParser = new Yargs();
  elevatedParser.scriptName("admin bob")
      .exitProcess(false)
      .recommendCommands()
      .command('react <messageId> <emoji>', 'reacts', {},
              argv => reactToMsg(argv.messageId, argv.emoji, argv.client))
      .command('výsledkofka', 'gets výsledkofka',{},
          async ({msg}) => await msg.channel.send(`\`\`\`${makeTable(await getRanking())}\`\`\``))
      .command('raw_výsledkofka', 'gets raw_výsledkofka',{},
          async ({msg}) => await msg.channel.send(`Ahoj ${msg.author}. Tu je tvoja výsledkofka.`, {
        files: ["./contestData.txt"]
      }))
      .command('messageUser <user> <what>', 'Sends a message to user', {}, argv => {
        let userMention = argv.msg.mentions.users.entries().next();
        if(!userMention.value || !argv.user.includes(userMention.value[1].id)) {
          logger.error("Usermention not found or it is not the same as command 'user' value.");
          return "zabudol si mention ty debil";
        }
        else{
          userMention.value[1].send(argv.what,createOptionObjectWithFilesAttached(argv.msg));
          argv.msg.react(nameToEmoji(":check_mark:"))
        }
      })
      .command('messageChannel <channel> <what>', 'Sends a message to channel', {}, argv => {
        let channelMention = argv.msg.mentions.channels.entries().next();
        if(!channelMention.value || !argv.channel.includes(channelMention.value[1].id)) {
          logger.error("Channelmention not found or it is not the same as command 'channel' value.");
          argv.msg.channel.send("zabudol si mention ty debil");
        }
        else{
          channelMention.value[1].send(argv.what,createOptionObjectWithFilesAttached(argv.msg));
          argv.msg.react(nameToEmoji(":check_mark:"))
        }
      })
      .command('vyjadriSa <toWhat>', 'Becomes S E N T I E N T', {}, async argv => {
        argv.msg.channel.send(await vyjadriSa(argv.toWhat))
      })
      .help();

  cipherParser = new Yargs();
  cipherParser.scriptName("šifrobob")
    .exitProcess(false)
    .recommendCommands()
    .command(['príchod <x> <y>','prichod'], 'príchod na miesto so súradnicami x a y', {},
      argv => {
      nearPoints = solutions
          .filter(solution => typeof solution.pos[0] === 'number' && typeof solution.pos[1] === 'number' )
          .map(solution=> ({
            dist:geolib.getDistance(
                {lat:solution.pos[0], lon: solution.pos[1]},
                {lat: parseFloat(argv.x), lon:parseFloat(argv.y)}
                ),
            solution
          }))
          .filter(({dist}) => dist<256)
          .map(({solution}) => argv.msg.reply(solution.text))
      if(nearPoints.length===0){
        argv.msg.reply("hmmm, vyzerá to že tu nič nie je :/")
      }
      //reactToMsg(argv.messageId, argv.emoji, argv.client)
      })
    .command(['hint <cisloHintu> k šifre <cisloUlohy>'], 'Bob vám poradí hint.', {}, argv => {
      relevantHints = hints[argv.cisloUlohy];
      if(!relevantHints || !relevantHints.hints || relevantHints.length === 0) {
        argv.msg.reply(`k danej úlohe nie sú dostupné hinty.`);
        return;
      }
      if(relevantHints.hints[argv.cisloHintu-1] === undefined) {
        argv.msg.reply(`k danej úlohe nie je dostupný hint ${argv.cisloHintu-1}`);
        return;
      }
      argv.msg.reply(`${argv.cisloHintu}. hint k ${argv.cisloUlohy}. úlohe: ${relevantHints.hints[argv.cisloHintu-1]}`)
    })
    .command('tajnyPrikaz', false, {}, argv => {
      argv.msg.reply('výborne, našiel si testovací tajný príkaz.')
    })
    .command('strasidlo', false, {}, argv => {
      argv.msg.reply('Výborne, zadaním správneho zaklínadla sa vám podarilo zistiť polohu prvého horcruxu. Jeho polohu opisuje nasledovná infromácia: Považská Bystrica Myjava. Na tom miesta sa nachádza aj ďalšia šifra, preto pripomíname, že sa nachádza do 30 km od Žriedlovej Doliny. Po nájdení správneho miesta zadajte šifrobobovi príchod ako zvyčajne. ')
    })
    .command('zradca', false, {}, argv => {
      argv.msg.reply('Výborne, ďalšia šifra sa nachádza v Starej Turej pred mestským úradom.')
    })
    .help()

});

client.on('message', async msg => {

  logger.info(`new message in ${msg.channel.name}`);

  if(msg.content.startsWith("/bobob:")){
    logger(`this is namespace for testing new functions. It won't probably trigger any other code.${msg.content}`);
    return;
  }

  if(process.env.DISABLED==="true"){
    logger.info("all functionality was disabled due to env variable DISABLED.");
    return;
  }

  if(msg.content.toLowerCase().startsWith("bob")){
    let finishMessageProcessing = false;
    plebsParser.parse(msg.content.slice("bob".length), {msg, client}, (err,argv,output) => {
      if(err){
        logger.error(`parser error: ${err}`);
        argv.msg.channel.send(`\`\`\`${err}\`\`\``);
      }
      if(output){
        logger.log(`parser output: ${output}`);
        argv.msg.channel.send(`\`\`\`${output}\`\`\``);
      }
      if(argv.didYargsRun||output) finishMessageProcessing = true;
    });
    if(finishMessageProcessing){
      return;
    }
  }


  if(msg.content.toLowerCase().startsWith("šifrobob")||msg.content.toLowerCase().startsWith("sifrobob")){
    let finishMessageProcessing = false;
    cipherParser.parse(msg.content.slice("šifrobob".length), {msg, client}, (err,argv,output) => {
      if(err){
        logger.error(`šifrobob error: ${err}`);
        argv.msg.channel.send(`\`\`\`${err}\`\`\``);
      }
      if(output){
        logger.log(`šifrobob output: ${output}`);
        argv.msg.channel.send(`\`\`\`${output}\`\`\``);
      }
      if(argv.didYargsRun||output) finishMessageProcessing = true;
    });
    if(finishMessageProcessing){
      return;
    }
  }


  if(msg.content.toLowerCase().startsWith("admin bob")&&msg.channel.name==="bot-test"){
    elevatedParser.parse(msg.content.slice("admin bob".length), {msg, client}, (err,argv,output) => {
      if(err){
        logger.error(`parser error: ${err}`);
        msg.channel.send(`\`\`\`${err}\`\`\``);
      }
      if(output){
        logger.log(`parser output: ${output}`);
        msg.channel.send(`\`\`\`${output}\`\`\``);
      }
    });
  }

  if (msg.channel.type !=="dm" && !msg.author.bot && msg.g) {
    if (await vikendoffka.handleMsg(msg, logger)) {
      return
    }
  }

  if((msg.channel.type==="dm"||msg.channel.name==="bot-test")&&!msg.author.bot){
    if(await runStories(msg).then(storiesRan => storiesRan.reduce((a,b) =>a||b))){
      return
    }
  }

  newUserHandler(msg);
  virtualExperiencesHandler(msg);

  if(msg.channel.type==="dm"&&!msg.author.bot){
    botTestChannels.array().map(async botTestChannel => {
      botTestChannel.send(`${msg.author} DM: ${msg.content}`,createOptionObjectWithFilesAttached(msg));
      if(msg.content.indexOf("submit")===0){
        await processSubmit(msg, botTestChannel)
      }
    })
  }
  postHypersHandler(msg, posts)

});

axiosModule();

cleanupModule(posts);