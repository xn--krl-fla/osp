const {nameToEmoji} = require("./emojis");

const fs = require("fs").promises;
const logger = require("./logger");
let channelHypers = {};
const minimalnyCas = 2; //v hodinach
const cakatPoBotovi = 8;
const randomCas = 1;

const postHypersHandler = async (msg, posts) => {
    if(msg.channel.type==="dm") return;
    if (channelHypers[msg.channel.id]) clearTimeout(channelHypers[msg.channel.id]);
    let lastMessage = await msg.channel.messages.fetch(msg.channel.messages.channel.lastMessageID);
    logger("creating new promise");
    new Promise((resolve, reject) => {
        const kolkoUbehloOdLast = (Date.now() - lastMessage.createdTimestamp) / 1000 / 3600;

        let when = Math.max((Math.random() + 1) * 1000 * 3600, 3600 * 1000 * (minimalnyCas - kolkoUbehloOdLast + (randomCas * Math.random() + cakatPoBotovi * lastMessage.author.bot)));
        logger(msg.channel.name, posts.channels);
        if (posts.channels[msg.channel.name] && posts.channels[msg.channel.name].posts.whenSilent && posts.channels[msg.channel.name].posts.whenSilent.length > 0) {
            console.log(`1234: Posielam správu do ${msg.channel.name} za ${when / 1000 / 3600}: ${JSON.stringify(posts.channels[msg.channel.name].posts.whenSilent)}`);
            channelHypers[msg.channel.id] = setTimeout(() => resolve(0), when);
        } else {
            reject(`no posts to post to channel ${msg.channel.name}`);
        }

    }).then(async () => {
        let post = popRandomElement(posts.channels[msg.channel.name].posts.whenSilent);
        if (post.type === "text") {

            console.log(`Posielam správu do ${msg.channel.name} teraz: ${post.content}`);
            return msg.channel.send(post.mock ? mockText(post.content) : post.content);
        }
        if (post.type === "image") {
            await msg.channel.send(`${post.content}`, {
                files: [
                    post.path
                ]
            })
        }
        if (post.type === "link") {
            await msg.channel.send(`${post.content}`, {
                files: [
                    post.path
                ]
            })
        }
        return postHypersHandler(msg.channel);
    }).catch(reason => logger.error(reason));
};

const virtualExperiencesHandler = async (msg) => {
    if (msg.channel.name === "bot-test" && !msg.author.bot) {        //TODO: this is shit my friend

        if (msg.content.indexOf("V I R T U Á L N") !== -1 && msg.content.indexOf("Z Á Ž I T") !== -1 && msg.content.toLowerCase().indexOf("bob") !== -1) {
            await msg.channel.send(`Ahoj ${msg.author}. Tu je tvoj V I R T U Á L N Y   Z Á Ž I T O K.`, {
                files: [
                    await fs.readdir('./bobsrc/channelContent/virtualExperiences').then((dirs) => `./bobsrc/channelContent/virtualExperiences/${dirs[Math.floor(Math.random() * dirs.length)]}`)
                ]
            });
        }
    }
};

const newUserHandler = (msg) => {
    if (msg.channel.name === "new-users" && !msg.author.bot) {
        msg.react(nameToEmoji(":check_mark:"))
        let message = `Ahoj ${msg.author}! Všimol som si, že si práve prišiel/la na náš Discord. Moje meno je Adam Král, som jeden z vedúcich. Programoval/la si už niekedy predtým? :)`;
        let timeout = 10000;
        logger.info(mockText(`Napísal som ${msg.author}.`));
        setTimeout(async () => await msg.author.send(message), timeout);
    }
}

module.exports = {postHypersHandler, virtualExperiencesHandler, newUserHandler};