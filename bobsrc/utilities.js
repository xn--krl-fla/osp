
const popRandomElement = (array) => array.splice(Math.floor(Math.random() * array.length), 1)[0];

const mockText = (text) => text.split('').map(x => Math.random() < 1 / 2 ? x.toLowerCase() : x.toUpperCase()).join('');

let contains = (text, what) => text.indexOf(what) !== -1;

const getRandomElement = (arr) => arr[Math.floor(Math.random() * arr.length)]


module.exports = { popRandomElement, mockText, contains, getRandomElement };