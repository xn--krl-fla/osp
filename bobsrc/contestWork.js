let fs = require("fs").promises;
let solutions = require("./eulerSolutions");
let AsciiTable = require('ascii-table');
const {contains} = require("./utilities");

let getRanking = async () => {
  let x = await fs.readFile("contestData.txt", "utf-8");

  let data = x.split("\n").map(x => x.split`,`.map(x => x.trim()));

  let ranks = {};

  data.forEach(([time, tag, authorId, submit]) => ranks[authorId] = {tag, score: 0, submits: {}});

  let weightFunction = x => 1;


  data.forEach(([time, tag, authorId, submit]) => {
    if (!submit) return;
    let parts = submit.split(" ").filter(x => x !== "");
    let order = parseInt(parts[1]);
    let result = parseInt(parts[2]);
    if (Number.isInteger(order) && result == solutions[order]) {
      if (!ranks[authorId].submits[order]) {
        ranks[authorId].submits[order] = result;
        ranks[authorId].score += weightFunction(order);
      }
    } else console.log(`Skipped submit ${submit} from ${tag}`)
  });
  fakeResults = [
    {
      tag: "B4RBER#1499",
      score: 39
    },
    {
      tag: "miro#2201",
      score: 4
    },
    {
      tag: "Sedem#7399",
      score: 8
    },
    {
      tag: "Sartreee#5616",
      score: 25
    },
    {
      tag: "MilosQo#2793",
      score: 21
    },
    {
      tag: "WellWellWell#6374",
      score: 5
    },
    {
      tag: "Shajter#2654",
      score: 31
    },
    {
      tag: "Jana22#2654",
      score: 10
    },
  ];
  return [...Object.values(ranks), ...fakeResults].sort((a, b) => b.score - a.score);

};

let makeTable = (vysledky) => {
  let table = new AsciiTable('Výsledkofka');
  vysledky.forEach(x => table.addRow(x.tag, x.score));
  return table;
};

const forbiddenChars = "!`'\"?;$#@\\";
let processSubmit = async (msg, botTestChannel) => {
  if (forbiddenChars.split("").map(ch => {
    if (contains(msg.content, ch)) {
      msg.reply(`Submit nemôže obsahovať znak "${ch}".`)
    }
    return contains(msg.content, ch)
  }).reduce((a, b) => a || b)) {
    botTestChannel.send(`${msg.author} sa práve pokúsil/la odovzdať submit s blbými znakmi. NEBUDU TO DĚLAT!`);
    return;
  }
  if (msg.content.length > 255) {
    msg.reply("Tvoj submit je dlhší ako 2^8 znakov. Pravdepodobne to nie je to čo chceš odovzdať.");
    botTestChannel.send(`${msg.author} sa práve pokúsil/la odovzdať submit dlhý ${msg.content.length} znakov. WTF? to tam mám vraziť do toho súboru? To aby si zapol LFS, debil.`);
    return;
  }

  await fs.appendFile('contestData.txt', `${Date.now()}, ${msg.author.tag}, ${msg.author.id}, ${msg.content}\n`);
  botTestChannel.send(`${msg.author} práve úspešne submittol ${msg.content}!`).then(msg => msg.delete({timeout: 10000}));
  let parts = msg.content.split(" ");
  msg.reply(`Submitol si úlohu ${parts[1]} s riešením ${parts[2]}, len tak ďalej. Po kontrole bude submit zarátaný do výsledkofky.`)

}

module.exports = {getRanking, makeTable, processSubmit};