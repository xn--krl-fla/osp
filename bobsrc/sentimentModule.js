/***************** sentiment analysis and translation *******************/


let Sentiment = require('sentiment');
let sentiment = new Sentiment();
const translate = require('@vitalets/google-translate-api');
const {mockText} = require("./utilities");
getTranslation = async (text) => {
  let translated = await translate(text, {to: 'en'});
  return translated.text;

};
getSentiment = async (text) => {
  return sentiment.analyze(await text);
};
vyjadriSa = async (text) =>{
  let sentimentResult = await getSentiment(getTranslation(text));
  if(sentimentResult.score===0) return mockText(text);
  if(sentimentResult.score>=0) return "Mám z toho úprimnú radosť!";
  if(sentimentResult.score<=0) return "to je mi úprimne ľúto."
};



module.exports = {vyjadriSa, getSentiment, getTranslation};