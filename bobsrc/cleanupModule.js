fs = require("fs").promises;

module.exports = (posts) =>{

  process.on('SIGINT', function () {
    console.log("Cleaning up, saving");

    (async () => {
      try {
        await fs.writeFile('bobsrc/channelContent/posts.json', JSON.stringify(posts, null, 2)); // need to be in an async function
        process.exit();
      } catch (error) {
        console.log(error)
      }
    })();
  });

};