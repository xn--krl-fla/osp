const logger = require('./logger');

module.exports = async (generalChannel, client) => {
  let testChannelSample = client.channels.cache.find(ch => ch.name === "testtest");
  let voiceTestChannel = client.channels.cache.find(ch => ch.name === "testvoice");

  let testTeam = {id: -0, name: "koberec8", members: ["Minuszero", "sombreroň", "invalidnamefgbixgfhs", "sobkulir"]}

  let findUser = (a) => {
    let member = generalChannel.guild.members.cache.find(
        x=>x.nickname === a||
            x.user.username === a||
            `${x.user.username}#${x.user.discriminator}` === a
    )
    if(!member) logger.error(`user ${a} not found.`);
    return member;
  }
  let createTeam = async (team) => {
    await generalChannel.guild.members.fetch();
    let channelName = `tím_${team.channelName||team.name}`;
    let voiceChannelName = `tím_${team.channelName||team.name}_voice`;
    let roleName = `tím ${team.name}`;
    let role = testChannelSample.guild.roles.cache.find(x => x.name === roleName) || (await testChannelSample.guild.roles.create({data: {name: roleName}}));
    let channel = testChannelSample.guild.channels.cache.find(x => x.name === channelName) || (await testChannelSample.guild.channels.create(channelName, {
      type: "text",
      parent: testChannelSample.parent,
      permissionOverwrites: testChannelSample.permissionOverwrites,
    }).then(channel => channel.updateOverwrite(role, {'SEND_MESSAGES': true, 'VIEW_CHANNEL': true})))
    let voiceChannel = voiceTestChannel.guild.channels.cache.find(x => x.name === voiceChannelName) || (await voiceTestChannel.guild.channels.create(voiceChannelName, {
      type: "voice",
      parent: voiceTestChannel.parent,
      permissionOverwrites: voiceTestChannel.permissionOverwrites,
    }).then(channel => channel.updateOverwrite(role, {'SEND_MESSAGES': true, 'VIEW_CHANNEL': true})))
    if (role.name !== roleName) logger.warn(`Name of the team should be edited as the role was renamed to "${role.name}". Original was"${roleName}"`)
    if (channel.name !== channelName) logger.warn(`Name of the team should be edited as the channel was renamed to "${channel.name}". Original was"${channelName}"`)
    if (voiceChannel.name !== voiceChannelName) logger.warn(`Name of the team should be edited as the voiceChannel was renamed to "${voiceChannel.name}". Original was"${voiceChannelName}"`)
    let members = team.members.map(m => findUser(m)).filter(m => !!m);
    logger.log(role.name, roleName, channel.name, channelName, members.length);
    logger.info(`adding ${members.length} players to team ${channelName} to role ${role.name}.`);
    members.map(m => {
      m.roles.add(role)
    });
    return {role, channel, voiceChannel, members}
  };
  return createTeam
}
