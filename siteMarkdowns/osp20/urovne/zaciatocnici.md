Ak si ešte neprogramoval/la alebo si chceš oprášiť to, čo už vieš, tak si v správnej kategórii!

### Čo mám robiť?

* Pripoj sa na [Discord](https://discord.gg/QKgNmW9)
* Nainštaluj si Python - [videonávod](https://www.youtube.com/watch?v=_oLcUfkgxSI)
* Čítaj [náš Python tutoriál](https://people.ksp.sk/~ajo/osp/python_tutorial.pdf)
(Tutoriál sme spísali nedávno a môže obsahovať chyby a nejasnosti. Ak ti niečo nie je jasné alebo máš návrh na zlepšenie napíš nám do kanálu [python_tutorial](https://discordapp.com/channels/598476317758849024/689027410272059422) na Discorde)
* Rieš [úlohy na testovači](https://testovac.ksp.sk/tasks/) v sekcii "Úvod do programovania". S odovzdávanim úloh na testovač si sa zrejme doteraz nestretol/la, pripravili sme teda pre teba [videonávod](https://www.youtube.com/watch?v=RV5uoLwQDsg)

Všetky videoprednášky sú pekne pokope v [zozname videoprednášok pre začiatočníkov](https://www.youtube.com/playlist?list=PLKwWTveCFVDvB3mlhVBYKPFDNWYSHpFpl).



## Zdroje

Najdôležitejším zdrojom sú [videotutoriály](https://www.youtube.com/watch?v=_oLcUfkgxSI&list=PLKwWTveCFVDvB3mlhVBYKPFDNWYSHpFpl) a [Python tutoriál](https://people.ksp.sk/~ajo/osp/python_tutorial.pdf). Ak o programovaní naozaj nič nevieš alebo vieš len málo, videá sú ten najlepší spôsob ako sa niečo nové naučiť. To isté platí aj ak si nikdy neskúšal odovzdávať na náš testovač. Python tutoriál v textovej podobe slúži na to aby si sa k obsahu z videotutoriálu vedel/la bližšie vrátiť a možno si prečítal/la aj nejaké detaily, ktoré prednášateľ nespomínal/la.

## Užitočné odkazy

[Python ťahák](https://people.ksp.sk/~ajo/letna_skola/cheatsheets/00-python-cheatsheet.pdf)

[Testovač s príkladmi](https://testovac.ksp.sk/)

## Pomocná literatúra

Rozsiahlejšie dielo [Python](https://www.smnd.sk/anino/moje/Python.pdf) od Anina Belana