# Čo mám robiť?
* Ak ešte nie si na Discord serveri Online Školy Programovania, poď na [Discord](https://discord.gg/QKgNmW9)! :)

* Prezri si zoznam dní, vyber si tému/úlohu ktorá ťa zaujme a pusti sa do riešenia. Potešíme sa keď nám dáš vedieť, ako ti to ide a radi ti pomôžeme s akýmkoľvek problémom.

# Dôležité linky

* [Discord](https://discord.gg/QKgNmW9) - komunikačný kanál, v ktorom ti vieme pomocť ak by si sa
  zasekol alebo odpovedať v prípade akýchkoľvek otázok
* [Python tutoriál (pdf)](https://people.ksp.sk/~ajo/osp/python_tutorial.pdf) a [Python
  prednášky](https://www.youtube.com/playlist?list=PLKwWTveCFVDtSan5sTsRyHuLc09SghpoQ) - základy
  programovania v Pythone
* [C++ prednášky](https://www.youtube.com/playlist?list=PLKwWTveCFVDtdtn2g1QqcO_6bGnioBcQW) -
  základy programovania v C++
* [Zoznam videoprednášok pre
  pokročilých](https://www.youtube.com/playlist?list=PLKwWTveCFVDsGvelaf5_CmEMSvitEhA09) - kanál, v
  ktorom postupne pribúdajú prednášky k jednotlivým témam
* [Vzorové riešenia](https://www.youtube.com/playlist?list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU) -
  vzorové riešenia jednotlivých úloh

  Ďalšie materiály nájdeš pri jednotlivých dňoch.

# Jednotlivé dni

Tu nájdeš program jednotlivých dní. Ich témy, prednášky, úlohy a vzoráky.

## Pondelok, 23.3.

Nové úlohy:

[dijkstra](https://testovac.ksp.sk/tasks/ls14-dijkstra/)

[space](https://testovac.ksp.sk/tasks/ls14-space/)

[zajko](https://testovac.ksp.sk/tasks/ls14-zajko/)

[brak](https://people.ksp.sk/~acm/problem.php?problem=brak)

### Linky a videá

* [KSP Kuchárka a kapitola o Dijkstrovom algoritme](https://www.ksp.sk/kucharka/dijkstra/)
* [Videoprednáška o Dijkstrovom algoritme](https://www.youtube.com/watch?v=vU-JKO-Xw3g&list=PLKwWTveCFVDsGvelaf5_CmEMSvitEhA09)

## Piatok - geometria

Dnes si vedúci Jano pripravil **prednášky o geometrii**, ktoré nájdeš v sekcii `Linky a Videá` nižšie.

### Linky a Videá

* [Prehľadný materiál k prednáškam, určite si ho pozri](https://www.ksp.sk/osp20/urovne/pokrocili/_plugin/attachments/download/644)
* [O základných geometrických pojmoch a priesečníkoch](https://bit.ly/2wnx2o0)
* [Ako počítať obsahy](https://bit.ly/2WtqmiN) 
* [O numpy a Jupyteri](https://bit.ly/3ahF1BX). Nemyslím Jupiter ako planétu, ale Jupyter - aplikácia na interaktívne výpočty, vizualizácie, grafy atď.

### Úlohy

<table style="border-spacing: 25px 0;margin-left: 25px;border: none; width: inherit;">
    <tbody>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-obvod ">Obvod</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-topenie ">Topenie</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-triangular ">Triangular</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-symetry">Symetry</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-obsah/">Obsah</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-tazisko/">Ťažisko</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-taziskovutvare/">Ťažisko v útvare</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    </tbody>
</table>

## Štvrtok - game development

Dnes je hlavný bod programu tvorba hier :) Ďalšie info nájdeš na
[ksp.sk/osp20/workshopy/game-development](https://ksp.sk/osp20/workshopy/game-development).

Ak by si ale mal záujem sa radšej zdokonalovať v programovaní, v kategorii `Úlohy` sú 2 úlohy na
precvičenie BFS.

### Úlohy

<table style="border-spacing: 25px 0;margin-left: 25px;border: none; width: inherit;">
    <tbody>
    <tr>
        <td style="border: none">
            <a href="https://people.ksp.sk/~acm/problem.php?contest=Skolske%20kolo%20zo%206.%20oktobra%202006&problem=crane">Žeriavy</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-brak/">Drak Brak</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    </tbody>
</table>



## Streda - úvod do grafových algoritmov

Dnešná sada je zameraná najmä na grafy a prehľadávanie do šírky (BFS). Odporúčame si preto pozrieť
prednášky o úvode do grafov, ich reprezentácii v pamäti a prehľadávaní do šírky.

### Linky a videá

* [Úvod do terminológie grafov](https://www.youtube.com/watch?v=G__oOESp-uA)
* [Reprezentácia grafov v pamäti](https://www.youtube.com/watch?v=8C-igXnIaaw)
* [Prehľadávanie do šírky](https://www.youtube.com/watch?v=scAVld8qrAI)
* [KSP Kuchárka](https://www.ksp.sk/kucharka/) _(kapitola Grafy)_
* [Kniha Pruvodce labyrintem algoritmu](http://pruvodce.ucw.cz/static/pruvodce.pdf) _(kapitola Základní grafové algoritmy)_

### Úlohy

<table style="border-spacing: 25px 0;margin-left: 25px;border: none; width: inherit;">
    <tbody>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls18-graphmatrix/">Matica susednosti</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls18-graphlists/">Zoznam susedov</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-susediasusedov/">Susedia susedov</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-najkratsiacesta/">Jednoduché BFS</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    <tr>
        <td style="border: none">
            <a href="https://testovac.ksp.sk/tasks/ls14-bludisko/">Bludisko na mriežke</a>
        </td>
        <td style="border: none">
            vzorák pribudne časom
        </td>
    </tr>
    </tbody>
</table>

## Utorok - knižničné funkcie

Tento deň by sme ti chceli predstaviť užitočné knižničné funkcie v Pythone aj C++, ktoré sa ti budú hodiť do budúcna, bez ohľadu na to, čo budeš programovať.
Odovzdávanie úloh je opäť podobné včerajšiemu postupu. V úlohe klikneš na tlačidlo vybrať súbor a následne vyberieš súbor v ktorom sa nachádza zdrojový
kód k danej úlohe. Následne stačí stlačiť tlačidlo odovzdať a po chvíľke strpenia sa dozvieš ako si dopadol/la.

### Videá

* [Python prednášky](https://www.youtube.com/watch?v=NU3BG59QmHo&list=PLKwWTveCFVDtSan5sTsRyHuLc09SghpoQ)
* [C++ prednášky](https://www.youtube.com/watch?v=F6XFQbUQtpU&list=PLKwWTveCFVDtdtn2g1QqcO_6bGnioBcQW)

### Úlohy

<table style="border-spacing: 25px 0;margin-left: 25px;border: none; width: inherit;">
    <tbody>
		<tr>
			<td style="border: none">
				<a href="https://people.ksp.sk/~acm/problem.php?contest=R%C3%BDchlostn%C3%A9%20vs.%20v%C3%BDberko%202009&problem=niedaleko">Jablko nepadá ďaleko od stromu</a>
			</td>
			<td style="border: none">
				<a href="https://www.youtube.com/watch?v=vS5kvIYlBxc&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=9">videovzorák</a>
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://people.ksp.sk/~acm/problem.php?contest=Trening%20z%2022.%20septembra%202006&problem=anagrams">Triedenie anagramov</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/jesko13-sort06/">Jabĺčka</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/jesko13-sort09/">Banková lúpež</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/ls14-queens/">Queens</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/ls17-studijne/">Rad na študijnom oddelení</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/ls17-prefix1/">Prefixove súčty</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/ls17-2D_prefix/">Viac Prefixovych súčtov</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/klub-pokemoni/">Pokémoni hrajú Dotu</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/klub-liska/">Tajomstvo líšky</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/ls17-simpleset1/">Simple set 1</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
		<tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/ls17-simpleset2/">Simple set 2</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
    <tr>
			<td style="border: none">
				<a href="https://testovac.ksp.sk/tasks/ls14-oddoneout/">Odd one out</a>
			</td>
			<td style="border: none">
				vzorák pribudne časom
			</td>
		</tr>
    </tbody>
</table>

## Pondelok - úvodná sada

Pripravili sme pre úvodnú teba sadu zaujímavých úloh. Na ich riešenie nepotrebuješ poznať žiaden algoritmus.

### Videá

* [Návod na načítavanie vstupu](https://www.youtube.com/watch?v=14KSJvpMRzw)
* [Návod na odovzdávanie programov](https://www.youtube.com/watch?v=RV5uoLwQDsg&t=2s)

### Úlohy

<table style="border-spacing: 25px 0;margin-left: 25px;border: none; width: inherit;">
    <tbody>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=%C4%BDahk%C3%BD%20%C3%BAvod%20do%20nov%C3%A9ho%20semestra&problem=trpaslik">Snehulienka a sedem trpaslíkov I.</a>
            </td>
            <td style="border: none">
                <a href="https://www.youtube.com/watch?v=NRvrWr_lIGQ&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=1">videovzorák</a>
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=Jesenn%C3%A1%202013&problem=delitelne9a">Číslo deliteľné 9</a>
            </td>
            <td style="border: none">
                <a href="https://www.youtube.com/watch?v=nS72gSsFGsM&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=2">videovzorák</a>
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=COCI%201-2008&problem=kengury">Kruté kengury</a>
            </td>
            <td style="border: none">
                <a href="https://www.youtube.com/watch?v=VNiQmcA-Jbw&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=4">videovzorák</a>
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?problem=bossfight">Boss fight</a>
            </td>
            <td style="border: none">
                <a href="https://www.youtube.com/watch?v=HsonSEVBSF4&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=3">videovzorák</a>
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=Farm%C3%A1rska&problem=bobuzly">Farmár Bob a uzly na lane</a>
            </td>
            <td style="border: none">
                <a href="https://www.youtube.com/watch?v=yk5nVgU6pmk&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=7">videovzorák</a>
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?problem=aviao">O avião</a>
            </td>
            <td style="border: none">
                vzorák pribudne časom
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=Tr%C3%A9ning%20z%2012.%20novembra%202010&problem=rodnecisla">Validácia rodných čísel</a>
            </td>
            <td style="border: none">
                <a href="https://www.youtube.com/watch?v=nj9_jg5ukiI&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=8">videovzorák</a>
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=Jesenn%C3%A1%202013&problem=delitelne9b">Číslo deliteľné 9 (ťažšia verzia)</a>
            </td>
            <td style="border: none">
                <a href="https://www.youtube.com/watch?v=lPpQ954mGuw&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=6">videovzorák</a>
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=Jesenn%C3%A1%202013&problem=vtaciky">Vtáčiky</a>
            </td>
            <td style="border: none">
                vzorák pribudne časom
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=%C5%A4a%C5%BE%C5%A1ia%20sada%20pred%20%C5%A1kolsk%C3%BDm%20kolom&problem=sach1">Nekonečná šachovnica</a>
            </td>
            <td style="border: none">
                vzorák pribudne časom
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=R%C3%BDchlostn%C3%A9%20vs.%20v%C3%BDberko%202012&problem=mechsort">Mechanický sort</a>
            </td>
            <td style="border: none">
                <a href="https://www.youtube.com/watch?v=yO1Pvhj8pZw&list=PLKwWTveCFVDsnRHUlsebC-vb3hazVI7MU&index=5">videovzorák</a>
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?contest=Vitame%20novy%20semester&problem=papricky">Logaritmické papričky</a>
            </td>
            <td style="border: none">
                vzorák pribudne časom
            </td>
        </tr>
        <tr>
            <td style="border: none">
                <a href="https://people.ksp.sk/~acm/problem.php?problem=zravce2">Väčšie žravce</a>
            </td>
            <td style="border: none">
                vzorák pribudne časom
            </td>
        </tr>
    </tbody>
</table>