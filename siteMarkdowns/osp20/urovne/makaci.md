
Čaute!
Na tejto stránke sa budú pridávať informácie ku skupine makačov  na Online Škole Programovania.

Vždy si si chcel/a nakódiť treap, ale nikdy sa ti nechcelo? Vďaka Online Škole Programovania máš na to jedinečnú príležitosť!

## Čo sa tu bude diať?

Každý deň-dva (uvidíme, ako dlho budú prázdniny pokračovať) zverejním výber úloh z rýchlostného programovania podľa dopytu vieme mať tematické (online, streamovanú, platformu uvidíme) prednášky.

Každý druhý výber úloh (číslujúc od nuly) bude sada z contestu, aby si si precvičil riešenie úloh, keď nevieš aká presne techniku treba použiť, ostatné výbery úloh budú zamerané na rôzne témy. Z aktuálnej sady tu budem priebežne publikovať výsledkovku.

Na Discorde budú vždy (počas rozumných denných hodín) prítomní vedúci, ktorí radi pomôžu (či s kódom, či dajú nejaký hint).

Taktiež sa v tomto dokumente nachádza zoznam nadchádzajúcich contestov (budú sa priebežne aktualizovať), ktoré odporúčam riešiť.

Ak si už všetky úlohy vyriešil/a, napíš na [Discord](https://discord.gg/QKgNmW9) Paulinke (@Paulinia) (alebo na email paulina.smolarova@trojsten.sk) a nájdem ti ďalšie :)

Niektoré problémy sú v angličtine, ak by si s ňou mal/a problém, tiež napíš Paulinke.

## Štvrtok - Codeforces contest

Dnes je na codeforces [contest](https://codeforces.com/contests/1326), ktorý nahradí zvyčajnú sadu úloh. Začína o 15:35 slovenského času, treba sa dopredu (5 minút minimálne) zaregistrovať na contest.

Úlohy bývajú zoradené od najľahšej po najťažšiu a počas contestu bývajú testované iba na pretestoch - takže ak ti niečo zbehne počas contestu, neznamená to, že riešenie je nutne správne, to zistíš až po konci (podobne ako na súťaži Zenit v programovaní). V kole vieš hackovať riešenia ostatných účastníkov - po submitnutí úlohy si vieš pozrieť ich riešenia a ak im nájdeš príklad, na ktorom im program nefunguje, vieš za to získať body.

## Streda - Začiatok novembra 2016 (04. November 2016):

Na stredu dávam do pozornosti tieto úlohy:

[brexit](https://people.ksp.sk/~acm/problem.php?contest=Za%C4%8Diatok%20novembra%202016&problem=brexit)

[cukraren](https://people.ksp.sk/~acm/problem.php?contest=Za%C4%8Diatok%20novembra%202016&problem=cukraren)

[halucradar](https://people.ksp.sk/~acm/problem.php?contest=Za%C4%8Diatok%20novembra%202016&problem=halucradar)

[roadreform](https://people.ksp.sk/~acm/problem.php?contest=Za%C4%8Diatok%20novembra%202016&problem=roadreform)

[zdvihacimost](https://people.ksp.sk/~acm/problem.php?contest=Za%C4%8Diatok%20novembra%202016&problem=zdvihacimost)

## Pondelok, Utorok - Stromové dátové štruktúry

Relevantné kuchárky:
[Intervalový strom](https://www.ksp.sk/kucharka/intervalovy_strom/)

[Lazy Intervalový Strom](https://www.ksp.sk/kucharka/lazy_intervalovy_strom/)

[Treap](https://www.ksp.sk/kucharka/treap/)

### Intervalové stromy
Ak si si ešte klasické intervalové stromy neprecvičil/a, tieto úlohy ti v tom isto pomôžu:

[napoleon](https://people.ksp.sk/~acm/problem.php?contest=&problem=napoleon) - minimový/maximový intervaláč

[poháriky](https://testovac.ksp.sk/tasks/ls16-pohariky/) - trikové riešenie

[bilboard](https://people.ksp.sk/~acm/problem.php?contest=NEERC%202008%20Northern%20Subregional&problem=billboard)

### Lazy Loading
Niekedy potrebujeme updatovať interval, vtedy pomôže takzvaný lazy-loading. Preskúšať si ho môžeš na tejto úlohe:

[miestenky](https://testovac.ksp.sk/tasks/ls14-miestenky/)

### Iné stromové štruktúry

[eustats](https://people.ksp.sk/~acm/problem.php?contest=R%C3%BDchlostn%C3%A9%20vs.%20v%C3%BDberko%202015&problem=eustats) - Perzistentný intervaláč - pamätanie si minulosti

[riecnyboj](https://people.ksp.sk/~acm/problem.php?contest=&problem=riecnyboj) - Skús si naprogramovať inú vyvažovanú štruktúru, napríklad Treap

[setmod](https://people.ksp.sk/~acm/problem.php?contest=%C5%A0kolsk%C3%A9%20kolo%20ACM%20ICPC%202014&problem=setmod) - naprogramuj si vyvažovaný strom ktorý vie nájsť k-tý element

[nicestplace](https://people.ksp.sk/~acm/problem.php?contest=%C5%A0kolsk%C3%A9%20kolo%20ACM%20ICPC%202012&problem=nicestplace) - vlastný vyvažovaný strom, vo spojení s ďalšou technikou 

## Vzoráky
Na niektoré úlohy sme nahrali vzoráky :) Nájdeš ich tu: https://www.youtube.com/watch?v=WX7L4aO-1SA&list=PLKwWTveCFVDs8Nn5hsDV3hSflOunbDlBs

## Nadchádzajúce contesty
[codeforces](https://codeforces.com/contests/1326) 19.3. 15:35. Na codeforces bývajú dvoj-dva a pol hodinové contesty. Úlohy sú zoradené podľa obtiažnosti a do dvoch divízií. Úlohy bývavajú zaujímavé a navyše môžete “hackovať” druhých ľudí - úlohy sú testované len na pretestoch, nie celých sadách, a po vyriešení úlohy si môžete prezerať riešenia iných ľudí a skúsiť im nájsť chybu.

[rýchlostné](https://people.ksp.sk/~acm/problems.php?curr) do 22.3. - Contest z rýchlostného programovania, na úlohy máte tri hodiny, ako prvé je hodnotený počet vyriešených úloh a sekundárne čas (a počet zlých submitov)

[Google Code Jam](https://codingcompetitions.withgoogle.com/codejam/schedule) 3.4.-5.4. Výročný contest organizovaný googlom. Po kvalifikácii (stačí získať 30 bodov na postup) nasledujú tri contesty prvého kola: 1A, 1B a 1C, z nich stačí riešiť jedno, obtiažnosť sa postupne sťažuje, ale kvalifikácia býva ľahká.

[Codechef](https://www.codechef.com/contests) March Cook-Off 22.3. 16:00-18:30, March LunchTime 28.3. 14:00-17:00 Indické contesty. Nemávajú úžasnú kvalitu, ale niekedy bývajú zaujímavé.

[Atcoder](https://atcoder.jp/contests/agc043) 21.3. 12:00 Japonské contesty, s podobným formátom ako codeforces. Bývajú dosť dobré, ale pomerne skoro.

## Výsledkovka za Stredu
| Meno     | Cukráreň | Brexit | Halucradar | Zdvíhací most | Road Reform |
|-------------|---------------|----------|------------------|-----------------------|---------------------|
| ELiška    | ✓         | ✓        |               |   ✓        |        |
| MisQo     | ✓         | ✓        |         |          |        |
| adam      | ✓         | ✓        |         |         |         |
| miskin001 | ✓         | ✓        | ✓        |          |       |
| Nadi      | ✓         | ✓        |         |          |         |
