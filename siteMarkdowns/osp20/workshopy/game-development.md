Dostal/la si sa na stránku nášho workshopu o tvorbe hier. Hry, ktoré sa budú na Online Škole tvoriť, majú pár špeciálnych pravidiel. Nájdeš ich popísané nižšie.

 Ak ťa baví vytvárať hry, ak ťa baví hry hrať alebo ak ťa baví v hrách hľadať skryté bonusy, na tejto stránke je pre teba vytvorený rebríček, v ktorom sa môžeš predbiehať s ostatnými účastníkmi/účastníčkami :) 
[image:646 align:right size:orig]
Na tejto stránke, ako aj na systéme zdieľania hier ešte stále pracujeme :) Preto nás neváhaj osloviť s akýmikoľvek nejasnosťami alebo návrhmi na  [Discord](https://discord.gg/QKgNmW9).

##Sieň slávy

### Hry

Ak si chceš pozrieť zoznam hier a zahrať si ich, stiahni si nášho herného klienta [Para™](https://github.com/sobkulir/para/releases). Môže sa v ňom objaviť aj tvoja hra! Stačí nám ju poslať do #game-development kanálu na Discorde. Para™ je dostupná pre väčšinu platforiem. 


### Rebríčky
<table style="border: none; width: inherit">
    <thead style="border: none">
        <tr style="border: none">
            <th style="border: none">
                Tvorcovia a tvorkyňe hier
            </th>
            <th style="border: none"></th>
        </tr>
    </thead>
    <tbody>
        <tr style="border: none">
            <td style="border: none">
                Matus240
            </td>
            <td style="border: none">
                1 hra
            </td>
        </tr>
        <tr style="border: none">
            <td style="border: none">
                Marienka
            </td>
            <td style="border: none">
                0 hier
            </td>
        </tr><tr style="border: none">
            <td style="border: none">
                Janko
            </td>
            <td style="border: none">
                0 hier
            </td>
        </tr>
    </tbody>
</table>


<table style="border: none; width: inherit">
    <thead style="border: none">
        <tr style="border: none">
            <th style="border: none">
                Najviac vyhratých hier
            </th>
            <th style="border: none"></th>
        </tr>
    </thead>
    <tbody style="border: none">
        <tr style="border: none">
            <td style="border: none">
                Marienka
            </td>
            <td style="border: none">
                0 hier
            </td>
        </tr>
        <tr style="border: none">
            <td style="border: none">
                Janko
            </td>
            <td style="border: none">
                0 hier
            </td>
        </tr>
    </tbody>
</table>


<table style="border: none; width: inherit">
    <thead style="border: none">
        <tr style="border: none">
            <th style="border: none">
                Najviac nájdených easter eggov
            </th>
            <th style="border: none"></th>
        </tr>
    </thead>
    <tbody style="border: none">
        <tr style="border: none">
            <td style="border: none">
                Marienka
            </td>
            <td style="border: none">
                0 hier
            </td>
        </tr>
        <tr style="border: none">
            <td style="border: none">
                Janko
            </td>
            <td style="border: none">
                0 hier
            </td>
        </tr>
    </tbody>
</table>





## Materiály k tvorbe hier

[Prednáška na nainštalovanie knižnice Easygame](https://youtu.be/KSbzDM4ckxQ)

[Dokumentácia knižnice easygame](https://github.com/faiface/easygame/blob/master/easygame.pdf)

[Obrázky jednotlivých herných objektov](https://drive.google.com/open?id=1wGtwvaHppmGJGY3JNZosFVbg2Sd2jfik)

[Ako vytvoriť svoju prvú jednoduchú hru v Pythone](https://youtu.be/LxDB4nxBSSI)

[Zoznam všetkých prednášok ku game developmentu](https://www.youtube.com/playlist?list=PLKwWTveCFVDvAT_1x6Gb0WSHpPXxjNiX9)

## Pravidlá hier

* Hry musia byť tematické, téma je **ovocie a zelenina**. To sa dá zakomponovať veľa spôsobmi - môžu to byť predmety ktoré hráč/ka zbiera, hráč/ka môže byť sám nejakým ovocím, hráč/ka môže [sekať lietajúce ovocie](https://fruitninja.com/), zelenina môže [bojovať proti zombie apokalypse](https://www.ea.com/games/plants-vs-zombies/plants-vs-zombies-2), atď.

* V hre **musí** byť hlavným cieľom získať zubnú pastu. Nie je to však také striktné, môžeš to zaobaliť napr. tak že hráč/ka za splnenie nejakého iného cieľa dostane zubnú pastu.

* Hry **musia** obsahovať Easter egg ([čo to je?](https://sk.wikipedia.org/wiki/Easter_egg)). Easter egg bude v tvojej hre predstavovať čokoláda - keď ješ tak veľa ovocia a zeleniny, určite sa potešíš keď náhodou nájdeš čokoládu. To sa dá tiež zaobaliť, napríklad po nájdení nejakého špeciálneho miesta zobrazíš užívateľovi/ke na obrazovku čokoládu.

* Okrem easter egg **môžu** hry obsahovať aj falošný easter egg. Keď ho hráč/ka nájde, bude si myslieť že našiel easter egg, no toto je len falošný! A čo je najznámejšia falošná čokoláda? Predsa biela čokoláda.

* Aby sme ti zarátali nájdenie hlavného cieľa alebo easter eggu, musíš urobiť snímku obrazovky (pomocou `Printscreen` tlačidla) a poslať nám ho na Discord.

* Okrem týchto špecifických pravidiel by hry mali dodržovať zásady všeobecnej slušnosti:
  * Tvojou hrou by si nemal urážať žiadne skupiny ľudí - vierovyznania, rasy, pohlavia, národu, orientácie atď. 
  * Nemalo by sa tam nadávať. 
  * Nemalo by tam byť nič, čo nie je vhodné pre najmladších na Online Škole Programovania (cca 10 rokov). Ak si nie si istý/á, či tam niečo môžeš dať, určite sa môžeš poradiť s vedúcimi na [Discorde](https://discord.gg/QKgNmW9), radi ti odpovieme na akékoľvek otázky.
  * Dobrý súbor pravidiel zásad slušnej spolupráce má Mozilla ([link](https://www.mozilla.org/en-US/about/governance/policies/participation/)).