V rámci plošného zatvorenia škôl a posunutia našich sústredení sme sa rozhodli vytvoriť náhradný program - **Online Škola Programovania**. Chceli by sme ti ponúknuť spôsob, ako sa cez toto obdobie nenudiť, a ešte sa aj pritom naučiť programovať, prípadne sa zlepšiť.

Online Škola Programovania je určená pre **základoškolákov a stredoškolákov**, pre ktorých máme nachystaných viacero kategórií. Každý si preto môže prísť na svoje, či už s programovaním úplne začínaš alebo si ostrieľaný programátor.

Po absolvovaní budeš vedieť naprogramovať svoju **vlastnú hru**, prinútiť počítač nakresliť niečo zaujímavé, naučíš sa, čo to je **algoritmicky myslieť** (a ako sa to robí) a hlavne sa zabavíš.

#### Celé dianie prebieha na Discorde Online Školy Programovania, čiže ak sa chceš zapojiť, neváhaj a príď na [Discord(odkaz)](https://discord.gg/QKgNmW9). Nejde ti pripojiť sa? Nižšie v sekcii [FAQ](https://www.ksp.sk/osp20/#wiki-toc-faq) nájdeš detailnejší návod.

# Čo mám robiť?

### Ešte si nikdy neprogramoval/la alebo si chceš oprášiť svoje vedomosti z programovania?

Máme pre teba potom pripravenú úroveň **začiatočníkov**.

* Pripoj sa na [Discord](https://discord.gg/QKgNmW9). Ak ti nejde sa pripojiť, nižšie v sekcii [FAQ](https://www.ksp.sk/osp20/#wiki-toc-faq) nájdeš detailnejší návod.
* Ak si úspešne pripojený/á, choď na [ksp.sk/osp20/urovne/zaciatocnici](https://ksp.sk/osp20/urovne/zaciatocnici), kde sa dozvieš všetky ďalšie informácie.

### Už vieš programovať a chceš sa zdokonaliť?

Máme pre teba potom pripravenú úroveň **pokročilých**.

* Pripoj sa na [Discord](https://discord.gg/QKgNmW9). Ak ti nejde sa pripojiť, nižšie v sekcii [FAQ](https://www.ksp.sk/osp20/#wiki-toc-faq) nájdeš detailnejší návod.
* Keďže každý deň sa bude robiť niečo iné, aktuálne inštrukcie nájdeš na stránke pre pokročilých: [ksp.sk/osp20/urovne/pokrocili/](https://www.ksp.sk/osp20/urovne/pokrocili/)

### Kolujú v tvojej krvi jednotky a nuly?

 Ak už zvládaš veci ako binárne vyhľadávanie, halda, BFS, alebo Dijkstra ľavou zadnou, máme pre teba pripravenú úroveň **makačov**. Táto úroveň je ale len pre poriadnych makačov!

* Pripoj sa na [Discord](https://discord.gg/QKgNmW9). Ak ti nejde sa pripojiť, nižšie v sekcii [FAQ](https://www.ksp.sk/osp20/#wiki-toc-faq) nájdeš detailnejší návod.
* Všetky ďalšie potrebné informácie sa dozvieš na [podstránke pre makačov](https://www.ksp.sk/osp20/urovne/makaci/).

# Workshopy

Okrem hlavného programu pre jednotlivé úrovne pripravujeme aj pútavé workshopy. Prvý z nich si pripravil Mišo Štrba, je **o tvorení hier** pre tých, ktorí ešte nikdy hry netvorili :) nájdeš ho tu: [Workshop o tvorení hier](https://ksp.sk/osp20/workshopy/game-development)

# FAQ

## Čo je discord? Ako sa pripojím?

Hlavné dianie prebieha na Discorde - Discord je aplikácia na online komunikáciu (chat a volania). Budú tam prítomní vedúci a ostatní účastníci. Keď sa pripojíš, môžeš s nimi komunikovať, pýtať sa na problémy, s ktorými sa stretneš počas riešenia úloh a sledovať ďalšie dianie. Pripojiť sa môžeš na [Discordovom kanáli Online Školy](https://discord.gg/QKgNmW9) - stránka sa ťa spýta na tvoje užívateľské meno, mail a heslo, a po krátkej registrácii už s nami môžeš komunikovať.

## Prečo Discord? 

Okrem samotného riešenia úloh sa na [Discorde](https://discord.gg/QKgNmW9) rozprávame aj o iných veciach, napríklad záujmy, pridávame tam memes, môžeš vidieť čo robia ostatní a rozprávať sa s inými v tvojom veku a s podobnými záujmami. Tiež si tam občas pokecáme na špeciálnych voice kanáloch - je to aspoň trochu náhrada za ľudský kontakt, ktorý nám tieto dni kvôli izolácii chýba. Ak chceš čokoľvek z toho robiť, neváhaj a pripoj sa! Porovnali sme viac alternatív, nakoniec však na tieto účely vyhral Discord.

## Kto sme?

Sme občianske združenie **Trojsten** a venujeme sa rozvoju a vzdelávaniu mládeže v oblastiach **matematiky, fyziky a informatiky**. Organizujeme množstvo aktivít, ktorých sa zúčastňuje približne 10 000 nadaných študentov ročne. Medzi tieto akcie patria najmä **tímové súťaže** Náboj a tri Letné školy, ktoré organizujeme počas letných prázdnin. Našou hlavnou náplňou je však organizácia troch korešpondenčných seminárov pre **študentov stredných škôl** ([FKS](https://fks.sk), [KMS](https://kms.sk) a [KSP](https://ksp.sk)) a dvoch pre **študentov základných škôl** ([UFO](https://ufo.fks.sk), [PRASK](https://prask.ksp.sk)), v rámci ktorých sa spolu viac ako 300 najlepších riešiteľov zúčastňuje jedenástich týždňových **sústredení**. Tieto sústredenia počas roka pripravuje skupina dobrovoľníkov, ktorí to robia vo svojom voľnom čase a bez nároku na odmenu.

## Pre rodičov: je toto vhodné miesto pre moje dieťa?

Diskusie na našom Discord serveri moderujeme a snažíme sa, aby to bolo bezpečné miesto pre všetkých. Máme dlhoročné skúsenosti s neformálnou výukou a prácou s deťmi (sústredenia, korešpondenčné semináre a súťaže). Ďalšie informácie o nás a čo robíme si môžete prečíťať vyššie v sekcii [Kto sme](https://www.ksp.sk/osp20/#wiki-toc-kto-sme) alebo na stránke [organizácie Trojsten](https://trojsten.sk).